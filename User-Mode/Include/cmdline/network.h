#ifndef _NETWORK_H_
#define _NETWORK_H_

//#include <bcrypt.h>
//#include <winnt.h>

NTSTATUS NetworkEntityInit(VOID);
NTSTATUS NetworkEntityUninit(VOID);

NTSTATUS NetworkBlockSite(CHAR *SiteName);
NTSTATUS NetworkBlockSiteOnSpecificApp(CHAR *SiteName, CHAR *ApplicationPath);

NTSTATUS NetworkUnblockSite(CHAR *SiteName);

#endif
