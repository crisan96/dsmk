#ifndef _CMDLINE_H_
#define _CMDLINE_H_

typedef unsigned __int64    QWORD;

#define WIN32_NO_STATUS
#include <Windows.h>
#undef WIN32_NO_STATUS

#define MAX_COMMAND_NAME_LENGTH 32
#define MAX_ARGUMENTS_LENGTH    32
#define MAX_CMD_LINE_LENGTH     MAX_COMMAND_NAME_LENGTH + MAX_ARGUMENTS_LENGTH

/**/ NTSTATUS   CmdLineInit(VOID);
/**/ NTSTATUS   CmdLineMatchAndExecuteCommand(CHAR *CmdLine);

#endif // !_CMDLINE_H_
