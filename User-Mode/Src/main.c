
#define WIN32_NO_STATUS
#include <Windows.h>
#undef WIN32_NO_STATUS

#include <ntstatus.h>

#include <winnt.h>
#include <winternl.h>

#include <stdio.h>
#include "cmdline/cmdline.h"

NTSTATUS main(VOID)
{
    NTSTATUS status = STATUS_SUCCESS;

    status = CmdLineInit();
    if (!NT_SUCCESS(status))
    {
        printf("Error: CmdLineInit failed with status = 0x%X!\n", status);
        goto cleanup;
    }

    while (TRUE)
    {
        CHAR inputCommand[MAX_CMD_LINE_LENGTH] = { 0 };
        printf("Type \'help\' for available commands.\n");
        printf("Command: ");

        fgets(inputCommand, MAX_CMD_LINE_LENGTH - 1, stdin);
        inputCommand[strlen(inputCommand) - 1] = 0; // Cut '\n'

        status = CmdLineMatchAndExecuteCommand(inputCommand);
        if (!NT_SUCCESS(status))
        {
            printf("Error! Your comand failed with status = 0x%X\n", status);
        }
    }

cleanup:
    return status;
}