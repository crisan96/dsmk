#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <ws2tcpip.h>
#include <stdio.h>
#include <fwpmu.h>
//#include <ntstatus.h>

typedef struct _SITE_BLOCKED
{
    CHAR    SiteName[MAX_PATH];

    struct
    {
        BOOLEAN         IsValid;
        CHAR            AppPath[MAX_PATH];

        BOOLEAN         NeedToFreeAppId;    // Ii un singur app id chiar daca site-ul are mai multe ip-uri => FREE numai odata
        FWP_BYTE_BLOB   *AppId;
    }SpecificAppBlocked;   // Valorile din structura sunt optionale (ori ambele setare ori niciuna)

    UINT64  BlockId;

    struct _SITE_BLOCKED *Next; // NULL => end of list
}SITE_BLOCKED;

typedef struct _NETWORK_GLOBAL_DATA
{
    BOOLEAN IsComponentInited;

    HANDLE EngineHandle;

    struct
    {
        SITE_BLOCKED ListHead;  // Lista de site-uri blocate de aplicatie
                                // Folosita cand vrem sa gasim ID-ul unui site blocat pentru a putea
                                // sa il deblocam. (cand se apeleaza Uninit ar trebui iterata lista si scoase
                                // siteurile blocate..)
    }SiteBlocked;
}NETWORK_GLOBAL_DATA;
static NETWORK_GLOBAL_DATA NetworkGlobalData;

/* Static functions */
static __forceinline BOOLEAN    _IsSiteAlreadyBlocked(CHAR *SiteName);
static __forceinline BOOLEAN    _IsSiteAlreadyBlockedOnSpecificApp(CHAR *SiteName, CHAR *AppName);
static __forceinline VOID       _AddBlockedSiteInGlobalList(CHAR *SiteName, UINT64 BlockId);
static __forceinline VOID       _AddBlockedSiteOnSpecificAppInGlobalList(CHAR *SiteName, UINT64 BlockId,
    CHAR *AppName, FWP_BYTE_BLOB *AppId, BOOLEAN NeedToFreeAppId);

NTSTATUS
NetworkEntityInit(
    VOID
)
{
    WSADATA wsaData;
    INT res = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (res != 0)
    {
        printf("WSAStartup failed: %d\n", res);
        return ((NTSTATUS)0xC0000001L);//STATUS_UNSUCCESSFUL;
    }

    DWORD result = FwpmEngineOpen0(
        NULL,
        RPC_C_AUTHN_WINNT,
        NULL,
        NULL,
        &NetworkGlobalData.EngineHandle
    );
    if (result != ERROR_SUCCESS)
    {
        printf("FwpmEngineOpen0 failed. Return value: %d.\n", result);
        return ((NTSTATUS)0xC0000001L);//STATUS_UNSUCCESSFUL;
    }

    NetworkGlobalData.SiteBlocked.ListHead.Next = NULL;

    NetworkGlobalData.IsComponentInited = TRUE;

    return ((NTSTATUS)0x00000000L);// STATUS_SUCCESS;
}

NTSTATUS
NetworkEntityUninit(
    VOID
)
{
    if(!NetworkGlobalData.IsComponentInited) return ((NTSTATUS)0x00000000L);// STATUS_SUCCESS;

    //
    // Deblocheaza site-urile blocate
    //
    SITE_BLOCKED *siteEntry = NetworkGlobalData.SiteBlocked.ListHead.Next;
    while (siteEntry != NULL)
    {
        // Unblock site
        DWORD result = FwpmFilterDeleteById0(
            NetworkGlobalData.EngineHandle,
            siteEntry->BlockId
        );
        if (result != ERROR_SUCCESS)
        {
            printf("FwpmFilterDeleteById0 failed with status = 0x%x\n", result);
        }

        // CHeck if we need to free the app id
        if (siteEntry->SpecificAppBlocked.IsValid && siteEntry->SpecificAppBlocked.NeedToFreeAppId)
        {
            FwpmFreeMemory0(&siteEntry->SpecificAppBlocked.AppId);
        }

        VOID *pointerToFree = (VOID *)siteEntry;
        siteEntry = siteEntry->Next;

        free(pointerToFree);
    }

    FwpmEngineClose0(NetworkGlobalData.EngineHandle);

    WSACleanup();

    return ((NTSTATUS)0x00000000L);// STATUS_SUCCESS;
}

NTSTATUS
NetworkBlockSite(
    CHAR *SiteName
)
{
    if (!NetworkGlobalData.IsComponentInited) return ((NTSTATUS)0xC0000001L);//STATUS_UNSUCCESSFUL;

    //
    // Vezi daca nu e cumva blocat site-ul..
    //
    if (_IsSiteAlreadyBlocked(SiteName))
    {
        printf("Site [%s] is already blocked!\n", SiteName);
        return ((NTSTATUS)0x00000000L);// STATUS_SUCCESS;
    }

    ADDRINFOA *result;
    INT getAddrInfoStatus = getaddrinfo(SiteName, NULL, NULL, &result);
    if (getAddrInfoStatus != 0)
    {
        printf("getaddrinfo failed, result = %d\n", getAddrInfoStatus);
        return ((NTSTATUS)0xC0000001L);//STATUS_UNSUCCESSFUL;
    }

    for (ADDRINFOA *currentResult = result; currentResult != NULL; currentResult = currentResult->ai_next)
    {
        if (currentResult->ai_family == AF_INET)
        {
            struct sockaddr_in *sockAddrIpv4 = (struct sockaddr_in *)currentResult->ai_addr;
            printf("\tIPv4 address %s\n",
                inet_ntoa(sockAddrIpv4->sin_addr));

            FWPM_FILTER0 filterSetup = { 0 };

            filterSetup.displayData.name = L"Site Blocker";
            filterSetup.displayData.description = L"Filter which block specific site.";

            filterSetup.flags = FWPM_FILTER_FLAG_NONE;

            filterSetup.layerKey = FWPM_LAYER_ALE_AUTH_CONNECT_V4;
            filterSetup.subLayerKey = IID_NULL;

            filterSetup.weight.type = FWP_EMPTY; // auto-weight.

            {
                FWPM_FILTER_CONDITION0 condition = { 0 };
                {
                    FWP_V4_ADDR_AND_MASK ipv4AddrMask = { 0 };
                    {
                        ipv4AddrMask.addr = ntohl(sockAddrIpv4->sin_addr.S_un.S_addr);
                        ipv4AddrMask.mask = 0xFFFFFFFF;
                    }

                    condition.fieldKey = FWPM_CONDITION_IP_REMOTE_ADDRESS;
                    condition.matchType = FWP_MATCH_EQUAL;
                    condition.conditionValue.type = FWP_V4_ADDR_MASK;
                    condition.conditionValue.v4AddrMask = &ipv4AddrMask;
                }

                filterSetup.numFilterConditions = 1;
                filterSetup.filterCondition = &condition;
            }

            filterSetup.action.type = FWP_ACTION_BLOCK;

            UINT64 blockId;
            DWORD blockAddResult = FwpmFilterAdd0(
                NetworkGlobalData.EngineHandle,
                &filterSetup,
                NULL,
                &blockId
            );
            if (blockAddResult != ERROR_SUCCESS)
            {
                printf("[ERROR] FwpmFilterAdd0 failed with status = [0x%x]\n", blockAddResult);
                // nu terminam functia, poate nu reuseste pe un ip dar reuseste pe altul :-??
            }
            else
            {
                // adauga noul site blocat in lista site-urilor blocate
                printf("Site [%s] is now BLOCKED!\n", SiteName);
                _AddBlockedSiteInGlobalList(SiteName, blockId);
            }
        }
    }

    freeaddrinfo(result);

    return ((NTSTATUS)0x00000000L);// STATUS_SUCCESS;
}

NTSTATUS
NetworkUnblockSite(
    CHAR *SiteName
)
{
    UNREFERENCED_PARAMETER(SiteName);
    // TODO
    return ((NTSTATUS)0xC0000002L);// STATUS_NOT_IMPLEMENTED
}

NTSTATUS
NetworkBlockSiteOnSpecificApp(
    CHAR *SiteName,
    CHAR *ApplicationPath
)
{
    if (!NetworkGlobalData.IsComponentInited) return ((NTSTATUS)0xC0000001L);//STATUS_UNSUCCESSFUL;

    //
    // Vezi daca nu e cumva blocat site-ul specific pe aplicatia data..
    //
    if (_IsSiteAlreadyBlockedOnSpecificApp(SiteName, ApplicationPath))
    {
        printf("Site [%s] is already blocked on the %s app!\n", SiteName, ApplicationPath);
        return ((NTSTATUS)0x00000000L);// STATUS_SUCCESS;
    }

    ADDRINFOA *result;
    INT getAddrInfoStatus = getaddrinfo(SiteName, NULL, NULL, &result);
    if (getAddrInfoStatus != 0)
    {
        printf("getaddrinfo failed, result = %d\n", getAddrInfoStatus);
        return ((NTSTATUS)0xC0000001L);//STATUS_UNSUCCESSFUL;
    }

    NTSTATUS status;
    FWP_BYTE_BLOB *appId;
    BOOLEAN isAtLeastOneEntryValid = FALSE;

    // Convert CHAR to WCHAR
    WCHAR appName[MAX_PATH + 1];
    size_t unused;
    mbstowcs_s(&unused, appName, sizeof(appName) / sizeof(WORD), ApplicationPath, MAX_PATH);

#define TEMP_NAME L"c:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"   // din cauza ca path-urile mai au
                                                                                        // spatii.... si separa aplicatia stringuri.... asa ca am hardcodat chrome...
    DWORD getAppInfoStatus = FwpmGetAppIdFromFileName0(TEMP_NAME, &appId);
    if (getAppInfoStatus != ERROR_SUCCESS)
    {
        printf("[ERROR] FwpmGetAppIdFromFileName0 failed with status = 0x%x\n", getAppInfoStatus);
        status = ((NTSTATUS)0xC0000001L);// STATUS_UNSUCCESSFUL
        goto cleanup;
    }

    for (ADDRINFOA *currentResult = result; currentResult != NULL; currentResult = currentResult->ai_next)
    {
        if (currentResult->ai_family == AF_INET)
        {
            struct sockaddr_in *sockAddrIpv4 = (struct sockaddr_in *)currentResult->ai_addr;
            printf("\tIPv4 address %s\n",
                inet_ntoa(sockAddrIpv4->sin_addr));

            FWPM_FILTER0 filterSetup = { 0 };

            filterSetup.displayData.name = L"Site Blocker";
            filterSetup.displayData.description = L"Filter which block specific site.";

            filterSetup.flags = FWPM_FILTER_FLAG_NONE;

            filterSetup.layerKey = FWPM_LAYER_ALE_AUTH_CONNECT_V4;
            filterSetup.subLayerKey = IID_NULL;

            filterSetup.weight.type = FWP_EMPTY; // auto-weight.

            {
                FWPM_FILTER_CONDITION0 conditions[2] = { 0 };
                {
                    //
                    // Prima conditie = IP-ul
                    //
                    FWP_V4_ADDR_AND_MASK ipv4AddrMask = { 0 };
                    {
                        ipv4AddrMask.addr = ntohl(sockAddrIpv4->sin_addr.S_un.S_addr);
                        ipv4AddrMask.mask = 0xFFFFFFFF;
                    }

                    conditions[0].fieldKey = FWPM_CONDITION_IP_REMOTE_ADDRESS;
                    conditions[0].matchType = FWP_MATCH_EQUAL;
                    conditions[0].conditionValue.type = FWP_V4_ADDR_MASK;
                    conditions[0].conditionValue.v4AddrMask = &ipv4AddrMask;

                    //
                    // A doua conditie = APP ID
                    //
                    conditions[1].fieldKey = FWPM_CONDITION_ALE_APP_ID;
                    conditions[1].matchType = FWP_MATCH_EQUAL;
                    conditions[1].conditionValue.type = FWP_BYTE_BLOB_TYPE;
                    conditions[1].conditionValue.byteBlob = appId;
                }

                filterSetup.numFilterConditions = 2;
                filterSetup.filterCondition = conditions;
            }

            filterSetup.action.type = FWP_ACTION_BLOCK;

            UINT64 blockId;
            DWORD blockAddResult = FwpmFilterAdd0(
                NetworkGlobalData.EngineHandle,
                &filterSetup,
                NULL,
                &blockId
            );
            if (blockAddResult != ERROR_SUCCESS)
            {
                printf("[ERROR] FwpmFilterAdd0 failed with status = [0x%x]\n", blockAddResult);
                // nu terminam functia, poate nu reuseste pe un ip dar reuseste pe altul :-??
            }
            else
            {
                // adauga noul site blocat in lista site-urilor blocate
                printf("Site [%s] is now BLOCKED on the app = [%s]!\n", SiteName, ApplicationPath);

                if (!isAtLeastOneEntryValid)
                {
                    _AddBlockedSiteOnSpecificAppInGlobalList(SiteName, blockId,
                        ApplicationPath, appId, TRUE);
                    isAtLeastOneEntryValid = TRUE;
                }
                else
                {
                    _AddBlockedSiteOnSpecificAppInGlobalList(SiteName, blockId,
                        ApplicationPath, appId, FALSE);
                }
            }
        }
    }

    status = ((NTSTATUS)0x00000000L);// STATUS_SUCCESS

cleanup:
    if(!isAtLeastOneEntryValid && getAppInfoStatus == ERROR_SUCCESS) FwpmFreeMemory0(&appId);
    if(getAddrInfoStatus == 0) freeaddrinfo(result);

    return status;
}

/* Static functions */
static
__forceinline
BOOLEAN
_IsSiteAlreadyBlocked(
    CHAR *SiteName
)
{
    SITE_BLOCKED *siteEntry = NetworkGlobalData.SiteBlocked.ListHead.Next;
    while (siteEntry != NULL)
    {
        if (strcmp(SiteName, siteEntry->SiteName) == 0) return TRUE;
        siteEntry = siteEntry->Next;
    }

    return FALSE;
}

static
__forceinline
BOOLEAN
_IsSiteAlreadyBlockedOnSpecificApp(
    CHAR *SiteName,
    CHAR *AppName
)
{
    SITE_BLOCKED *siteEntry = NetworkGlobalData.SiteBlocked.ListHead.Next;
    while (siteEntry != NULL)
    {
        if (
            (strcmp(SiteName, siteEntry->SiteName) == 0)
            && ((siteEntry->SpecificAppBlocked.IsValid)
                && (strcmp(AppName, siteEntry->SpecificAppBlocked.AppPath) == 0))
            )
        {
            return TRUE;
        }

        siteEntry = siteEntry->Next;
    }

    return FALSE;
}

static
__forceinline
VOID
_AddBlockedSiteInGlobalList(
    CHAR* SiteName,
    UINT64 BlockId
)
{
    //
    // Aloca si initializeaza noua intrare
    //
    SITE_BLOCKED *newEntry = (SITE_BLOCKED *)malloc(sizeof(newEntry[0]));
    if (newEntry == NULL) return;

    strcpy_s(newEntry->SiteName, MAX_PATH, SiteName);
    newEntry->BlockId = BlockId;
    newEntry->Next = NULL;

    newEntry->SpecificAppBlocked.IsValid = FALSE;

    //
    // Daca lista este goala, aceasta va fi prima intrare
    //
    if (NetworkGlobalData.SiteBlocked.ListHead.Next == NULL)
    {
        NetworkGlobalData.SiteBlocked.ListHead.Next = newEntry;
        return;
    }

    //
    // Gaseste ultima intrare.
    //
    SITE_BLOCKED *lastEntry = NetworkGlobalData.SiteBlocked.ListHead.Next;
    while (lastEntry->Next != NULL)
    {
        lastEntry = lastEntry->Next;
    }
    lastEntry->Next = newEntry;

    return;
}

static
__forceinline
VOID
_AddBlockedSiteOnSpecificAppInGlobalList(
    CHAR *SiteName,
    UINT64 BlockId,
    CHAR *AppPath,
    FWP_BYTE_BLOB *AppId,
    BOOLEAN NeedToFreeAppId
)
{

    //
    // Aloca si initializeaza noua intrare
    //
    SITE_BLOCKED *newEntry = (SITE_BLOCKED *)malloc(sizeof(newEntry[0]));
    if (newEntry == NULL) return;

    strcpy_s(newEntry->SiteName, MAX_PATH, SiteName);
    newEntry->BlockId = BlockId;
    newEntry->Next = NULL;

    newEntry->SpecificAppBlocked.IsValid = TRUE;
    newEntry->SpecificAppBlocked.NeedToFreeAppId = NeedToFreeAppId;
    strcpy_s(newEntry->SpecificAppBlocked.AppPath, MAX_PATH, AppPath);
    newEntry->SpecificAppBlocked.AppId = AppId;

    //
    // Daca lista este goala, aceasta va fi prima intrare
    //
    if (NetworkGlobalData.SiteBlocked.ListHead.Next == NULL)
    {
        NetworkGlobalData.SiteBlocked.ListHead.Next = newEntry;
        return;
    }

    //
    // Gaseste ultima intrare.
    //
    SITE_BLOCKED *lastEntry = NetworkGlobalData.SiteBlocked.ListHead.Next;
    while (lastEntry->Next != NULL)
    {
        lastEntry = lastEntry->Next;
    }
    lastEntry->Next = newEntry;

    return;
}