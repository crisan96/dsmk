#include "cmdline/cmdline.h"
#include <ntstatus.h>
#include <winternl.h>
#include <stdio.h>
#include <psapi.h>
#include "common.h"
#include "fltuser.h"
#include <wchar.h>
#include "cmdline/network.h"

typedef struct _CMD_GLOBALD_DATA
{
    struct
    {
        HANDLE CommPortHandle;          // Folosit pentru a comunica cu minifiltrul

        HANDLE ListenerThreadHandle;    // Handle la thread-ul ce consuma mesajele
                                        // (practic doar printeaza informatiile despre
                                        // procese, threaduri, registrii etc ce vin de la minifiltru).

        HANDLE ListenerThreadHandleTerminationEvent;

    }MiniFilter;
    UINT32 Reserved;
}CMD_GLOBAL_DATA;
static CMD_GLOBAL_DATA CmdLineGlobalData;

//
// What the command handler function looks like
//
typedef NTSTATUS(*PFUNC_CommandHandler)(DWORD Argc, CHAR **Argv);

typedef struct _COMMAND
{
    CHAR                    CommandName[MAX_COMMAND_NAME_LENGTH];
    CHAR                    *HelpMessage;
    PFUNC_CommandHandler    CommandHandler;
}COMMAND;

/* Static functions */
static VOID     _PrintProcessNameAndId(DWORD ProcessId);
static VOID     _PrintProcessCmdLine(DWORD ProcessId);

/* Mini-filter specific functions */
static DWORD WINAPI _MiniFilterConsumerThread(_In_ LPVOID Parameter);

/* Available commands */
static NTSTATUS CmdHelp(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdExit(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdPrintAllRunningProcesses(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdCallFirstDriverFunction(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdCallSecondDriverFunction(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdProtectProcess(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdPingFsMiniFilter(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStartMonitoringProcesses(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStopMonitoringProcesses(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStartMonitoringThreads(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStopMonitoringThreads(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStartMonitoringLoadImage(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStopMonitoringLoadImage(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStartMonitoringRegistry(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStopMonitoringRegistry(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStartMonitoringFiles(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdStopMonitoringFiles(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdMonitorFileDelete(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdSetDirectoryProtection(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdBlockSite(DWORD Argc, CHAR **Argv);
static NTSTATUS CmdBlockSiteOnSpecificApp(DWORD Argc, CHAR **Argv);

static COMMAND CommandLineCommands[] =
{
    {
        "help",
        "Print all the commands and help message for them",
        CmdHelp
    },
    {
        "listprocs",
        "Disaplay all processes running.\n\
            Usage: listprocs [print-cmd-line]\n\
            where   [print-cmd-line] :  true => prints process cmd line\n\
                                        false => do not prints process cmd line",
        CmdPrintAllRunningProcesses
    },
    {
        "lab3-1-1",
        "lab3-first-IOCTL",
        CmdCallFirstDriverFunction
    },
    {
        "lab3-1-2",
        "lab3-second-IOCTL",
        CmdCallSecondDriverFunction
    },
    {
        "protect",
        "Protect a process with a given PID from termination.\n\
            Usage: protect 2010",
        CmdProtectProcess
    },
    {
        "ping-mini-filter",
        "Ping our loaded FileSystem MiniFilter (if loaded...). No arguments\n\
            Usage: ping-mini-filter",
        CmdPingFsMiniFilter
    },
    {
        "mini-filter-start-process",
        "Start monitoring process creation and termination. No parameters",
        CmdStartMonitoringProcesses
    },
    {
        "mini-filter-stop-process",
        "Stop monitoring process creation and termination. No parameters",
        CmdStopMonitoringProcesses
    },
    {
        "mini-filter-start-threads",
        "Start monitoring threads creation and termination. No parameters",
        CmdStartMonitoringThreads
    },
    {
        "mini-filter-stop-threads",
        "Stop monitoring threads creation and termination. No parameters",
        CmdStopMonitoringThreads
    },
    {
        "mini-filter-start-ldimg",
        "Start monitoring load images in UM processes or in kernel. No parameters",
        CmdStartMonitoringLoadImage
    },
    {
        "mini-filter-stop-ldimg",
        "Stop monitoring load images in UM processes or in kernel. No parameters",
        CmdStopMonitoringLoadImage
    },
    {
        "mini-filter-start-regs",
        "Start monitoring registry keys. No parameters",
        CmdStartMonitoringRegistry
    },
    {
        "mini-filter-stop-regs",
        "Stop monitoring registry keys. No parameters",
        CmdStopMonitoringRegistry
    },
    {
        "mini-filter-start-files",
        "Start monitoring files operation. No parameters",
        CmdStartMonitoringFiles
    },
    {
        "mini-filter-stop-files",
        "Stop monitoring files operation. No parameters",
        CmdStopMonitoringFiles
    },
    {
        "mini-filter-delete-monitor",
        "Start monitoring files delete. No parameters",
        CmdMonitorFileDelete
    },
    {
        "mini-filter-dir-protect",
        "Enable or disable write file protection on the given directory.\n\
            Usage: mini-filter-dir-protect <mode> <dir-path>\n\
            where:  <mode> = enable -> enable protection\n\
                             disable -> disable protection\n\
                    <dir-path> = directory to protect/unprotect path\n\
        Example: mini-filter-dir-protect enable c:\\test\\ ",
        CmdSetDirectoryProtection
    },
    {
        "block-site",
        "Blocks a site from being accessed.\n\
            Usage: block-site <site>\n\
         Example: block-site www.yahoo.com",
        CmdBlockSite
    },
    {
        "block-site-on-app",
        "Blocks a site from being accessed on a specific applications.\n\
            Usage: block-site <site> <app-name>\n\
         Example: block-site www.yahoo.com chrome.exe",
        CmdBlockSiteOnSpecificApp
    },
    {
        "exit",
        "Close the application",
        CmdExit
    },
};
#define NUMBER_OF_COMMANDS ARRAYSIZE(CommandLineCommands)
static_assert(NUMBER_OF_COMMANDS < MAXDWORD, "Too many commands!"); // We use an DWORD variable to
                                                                    // iterate commands. So, there can be more commands

/**/
NTSTATUS
CmdLineInit(
    VOID
)
{
    //
    // Initializam mecanismul de comunicare cu mini filter-ul
    //
    HRESULT result = FilterConnectCommunicationPort(
        FS_MINIFILTER_COMM_PORT,
        0,                          // default usage is synchronous
        NULL,                       // keep it simple..no context
        0,
        NULL,                       // default security attr
        &CmdLineGlobalData.MiniFilter.CommPortHandle
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterConnectCommunicationPort failed! (Maybe mini filter not loaded yet..?\n");
        CmdLineGlobalData.MiniFilter.CommPortHandle = INVALID_HANDLE_VALUE;
        // Nu failam aplicatia in caz ca nu reusim.
        // Note: CloseHandle se face in comanda de exit (singur-ul path legit de iesire din app)
    }

    //
    // Si sa nu uitam de thread-ul ce sta numa
    // si asteapta mesaje de la driver.
    // Acest driver va fi practic cel care consuma
    // mesajele ce contin informatiile despre procese,
    // threaduri, registrii etc.
    //
    CmdLineGlobalData.MiniFilter.ListenerThreadHandleTerminationEvent =
        CreateEvent(NULL, TRUE, FALSE, NULL);
    if (CmdLineGlobalData.MiniFilter.ListenerThreadHandleTerminationEvent == NULL)
    {
        printf("[ERROR] CreateEvent failed! GetLastErr = [0x%x]\n", GetLastError());
        return STATUS_SUCCESS;  // Nu mai continua cu crearea de thread...
                                // returnam succes pt ca aplicatia nu se bazeaza doar pe comm cu FsMinifilter
                                // (poate trebuia void functia atunci mai bine..)
    }

    CmdLineGlobalData.MiniFilter.ListenerThreadHandle = CreateThread(
        NULL,                       // default security attr
        0,                          // default stack size
        _MiniFilterConsumerThread,
        NULL,                       // no parameters
        0,                          // thread run immediate after creation
        NULL                        // no need for thread id
    );
    if (CmdLineGlobalData.MiniFilter.ListenerThreadHandle == NULL)
    {
        printf("[ERROR] CreateThread failed! GetLastErr = [0x%x]\n", GetLastError());

        if (CmdLineGlobalData.MiniFilter.CommPortHandle != INVALID_HANDLE_VALUE)
        {
            CloseHandle(CmdLineGlobalData.MiniFilter.CommPortHandle);
            CmdLineGlobalData.MiniFilter.CommPortHandle = INVALID_HANDLE_VALUE;
        }
    }

    //
    // Init network entity
    //
    NTSTATUS status = NetworkEntityInit();
    if (!NT_SUCCESS(status))
    {
        printf("[ERROR] NetworkEntityInit failed! status = [0x%x]\n", status);
    }

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
CmdLineMatchAndExecuteCommand(
    CHAR *CmdLine
)
{
#define MAX_ARGC 20
    QWORD length = strlen(CmdLine);

    if (!CmdLine) { return STATUS_INVALID_PARAMETER_1; }

    if (length >= MAX_CMD_LINE_LENGTH) { return STATUS_INVALID_PARAMETER_1; }

    DWORD argc = 0;
    CHAR *argv[MAX_ARGC] = { NULL };
    DWORD index = 0;
    while (TRUE)
    {
        // Skip leading spaces
        while ((CmdLine[index] == ' ') && (index < length)) { ++index; }

        // All the (remaining) cmdline was just spaces
        // or we reach the end of cmdline.
        // Stop processing if no more chars.
        if (index >= length) { break; }

        // Found argv
        argv[argc++] = &(CmdLine[index]);

        if (argc >= MAX_ARGC) { break; }

        // Skip current argv until we got a space or end of cmdline
        while ((CmdLine[index] != ' ') && index < length) { ++index; }
        CmdLine[index] = 0;
        ++index;
    }

    CHAR *commandName = argv[0];
    if (!commandName) { return STATUS_NOT_FOUND; }

    for (DWORD i = 0; i < NUMBER_OF_COMMANDS; ++i)
    {
        if (strcmp(commandName, CommandLineCommands[i].CommandName) == 0)
        {
            // Command matched. Execute its handler
            // Sent just parameters, we dont need the command name anymore
            return CommandLineCommands[i].CommandHandler(argc - 1, &argv[1]);
        }
    }

    return STATUS_NOT_FOUND;
}

/* Static functions */
static
VOID
_PrintProcessNameAndId(
    DWORD ProcessId
)
{
    WCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");

    // Get a handle to the process.
    HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                  PROCESS_VM_READ,
                                  FALSE, ProcessId );

    if (!hProcess) { return; }

    // Get the process name.
    {
        HMODULE hMod;
        DWORD cbNeeded;

        if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod),
            &cbNeeded) )
        {
            GetModuleBaseName( hProcess, hMod, szProcessName,
                              sizeof(szProcessName)/sizeof(TCHAR) );
        }
    }

    // Print the process name and identifier.
    printf( ("%ws  (PID: %u)\n"), szProcessName, ProcessId );

    // Release the handle to the process.
    CloseHandle( hProcess );
}

static
VOID
_PrintProcessCmdLine(
    DWORD ProcessId
)
{
    // NtQueryInformationProcess has no associated import library.
    // You must use the LoadLibrary and GetProcAddress functions to dynamically link to Ntdll.dll.
    typedef __kernel_entry NTSTATUS(*PFUNC_NtQueryInformationProcess)(
        IN HANDLE           ProcessHandle,
        IN PROCESSINFOCLASS ProcessInformationClass,
        OUT PVOID           ProcessInformation,
        IN ULONG            ProcessInformationLength,
        OUT PULONG          ReturnLength
    );

    WCHAR *bufferCopy = NULL;
    PEB *pebStructureCopy = NULL;
    RTL_USER_PROCESS_PARAMETERS *rtlProcParamCopy = NULL;

    HMODULE handleDll = LoadLibraryA("Ntdll.dll");
    if (!handleDll) { goto cleanup; }

    PFUNC_NtQueryInformationProcess DynNtQueryInformationProcess = (PFUNC_NtQueryInformationProcess)GetProcAddress(handleDll, "NtQueryInformationProcess");
    if (!DynNtQueryInformationProcess) { goto cleanup; }

    // Get a handle to the process.
    HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                  PROCESS_VM_READ,
                                  FALSE, ProcessId );
    if (!hProcess) { goto cleanup; }

    // Retrieves a pointer to a PEB structure
    ULONG unused;
    PROCESS_BASIC_INFORMATION processBasicInformation;
    if (!NT_SUCCESS(DynNtQueryInformationProcess(
        hProcess,
        ProcessBasicInformation,    // Retrieves a pointer to a PEB structure
        &processBasicInformation,
        sizeof(processBasicInformation),
        &unused
        )))
    {
        goto cleanup;
    }

    PEB *targetPebStructure = processBasicInformation.PebBaseAddress;
    pebStructureCopy = (PEB*)malloc(sizeof(pebStructureCopy[0]));
    if (!pebStructureCopy) { goto cleanup; }

    if (!ReadProcessMemory(
        hProcess,
        targetPebStructure,
        pebStructureCopy,
        sizeof(pebStructureCopy[0]),
        NULL
        ))
    {
        goto cleanup;
    }

    RTL_USER_PROCESS_PARAMETERS *targetRtlProcParam = pebStructureCopy->ProcessParameters;
    rtlProcParamCopy = (RTL_USER_PROCESS_PARAMETERS*)malloc(sizeof(rtlProcParamCopy[0]));
    if (!rtlProcParamCopy) { goto cleanup; }

    if (!ReadProcessMemory(
        hProcess,
        targetRtlProcParam,
        rtlProcParamCopy,
        sizeof(rtlProcParamCopy[0]),
        NULL
        ))
    {
        goto cleanup;
    }

    WCHAR *targetBuffer = rtlProcParamCopy->CommandLine.Buffer;
    USHORT targetLength =  rtlProcParamCopy->CommandLine.Length;
    bufferCopy = (WCHAR*)malloc(targetLength * sizeof(WCHAR));
    if (!bufferCopy) { goto cleanup; }

    if (!ReadProcessMemory(
        hProcess,
        targetBuffer,
        bufferCopy, // command line goes here
        targetLength,
        NULL))
    {
        goto cleanup;
    }

    printf("cmdLine = %ws\n", bufferCopy);

cleanup:
    if (bufferCopy)         { free(bufferCopy); }
    if (rtlProcParamCopy)   { free(rtlProcParamCopy); }
    if (pebStructureCopy)   { free(pebStructureCopy); }
    if (handleDll)          { FreeLibrary(handleDll); }

    return;
}

/* Mini-filter specific functions */
static
DWORD
WINAPI
_MiniFilterConsumerThread(
    _In_ LPVOID Parameter
)
{
    UNREFERENCED_PARAMETER(Parameter);

    VOID *msgBuffer = malloc(sizeof(FILTER_MESSAGE_HEADER) + sizeof(DATA_PACKET));
    if (msgBuffer == NULL) return 1;

    // Pentru ca functia de FilterGetMessage
    // sa nu fie blocanta. De ce? Pentru a putea
    // verifica in acelasi timp si evenimentul ce
    // ne spune ca trebuie sa terminam thread-ul.
    OVERLAPPED overlapped = { 0 };
    HANDLE handleOverlappedEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (handleOverlappedEvent == NULL) return 2;
    overlapped.hEvent = handleOverlappedEvent;
    overlapped.Offset = overlapped.OffsetHigh = 0;

    // Cele 2 handle-uri dupa care thread-ul asteapta
    HANDLE waitEvents[2];
    waitEvents[0] =     // cand trebuie sa terminam threadul
        CmdLineGlobalData.MiniFilter.ListenerThreadHandleTerminationEvent;
    waitEvents[1] =     // cand am primit un mesaj
        handleOverlappedEvent;

    BOOLEAN threadMustFinish = FALSE;
    while (!threadMustFinish)
    {
        NTSTATUS waitStatus;
        BOOLEAN didWeReallyWaitForOverlapped = FALSE;

        HRESULT result = FilterGetMessage(
            CmdLineGlobalData.MiniFilter.CommPortHandle,
            msgBuffer, sizeof(FILTER_MESSAGE_HEADER) + sizeof(DATA_PACKET),
            &overlapped
        );
        if (result == S_OK)
        {
            // we managed to get a message
            waitStatus = STATUS_WAIT_0 + 1; // corresponds to signaling of the hOverlappedEvent

        }
        else if ((result & 0x7FFF) == ERROR_IO_PENDING)
        {
            // we have to wait for messages
            didWeReallyWaitForOverlapped = TRUE;
            waitStatus = WaitForMultipleObjects(2, waitEvents, FALSE, INFINITE);
        }
        else if ((result & 0x7FFF) == ERROR_INVALID_HANDLE)
        {
            // The communication port was closed
            // This should never happen while the service is active
            // terminating the thread
            waitStatus = STATUS_WAIT_0;
        }
        else
        {
            // unexpected status returned
            waitStatus = FILTER_FLT_NTSTATUS_FROM_HRESULT(result);
            wprintf(L"[Error] FilterGetMessage returned unexpected result. Status = 0x%X\n", waitStatus);
            continue;
        }

        if (waitStatus == STATUS_WAIT_0)
        {
            // Trebe sa terminam thread-ul..
            // cleanup-ul (la comunicare) va fi facut in cadrul comenzii de exit
            threadMustFinish = TRUE;
            continue;
        }
        else if (waitStatus == STATUS_WAIT_0 + 1)
        {
            //
            // message received
            //
            if (didWeReallyWaitForOverlapped)
            {
                DWORD dwBytesWritten;
                if (!GetOverlappedResult(CmdLineGlobalData.MiniFilter.CommPortHandle,
                    &overlapped, &dwBytesWritten, FALSE))
                {
                    wprintf(L"[Error] GetOverlappedResult GetLastError = 0x%X\n", GetLastError());
                    continue;
                }
            }

            DATA_PACKET *dataPacket = (DATA_PACKET *)((BYTE *)msgBuffer + sizeof(FILTER_MESSAGE_HEADER));
            printf("[%s] %S\n", __FUNCTION__, dataPacket->Parameters.ConsumerThread.Message);
        }
        else
        {
            // something failed
            wprintf(L"[Error] WaitForMultipleObjects returned unexpected result. Status = 0x%X\n", waitStatus);
        }

    }
    if (handleOverlappedEvent) CloseHandle(handleOverlappedEvent);
    if (msgBuffer) free(msgBuffer);


    return 0;
}

/* Available commands */
static
NTSTATUS
CmdHelp(
    DWORD   Argc,
    CHAR    **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    // Help command does not have parameters
    if (Argc != 0) { return STATUS_INVALID_PARAMETER; }

    for (DWORD commandIndex = 0; commandIndex < NUMBER_OF_COMMANDS; ++commandIndex)
    {
        printf(" - %s -> %s\n",
               CommandLineCommands[commandIndex].CommandName,
               CommandLineCommands[commandIndex].HelpMessage);
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdExit(
    DWORD   Argc,
    CHAR    **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    // Exit command does not have parameters
    if (Argc != 0) { return STATUS_INVALID_PARAMETER; }

    // Call here all terminate functions. Ex: when threadpool is added, it's terminate function
    // ....

    //
    // Network uninit
    //
    NetworkEntityUninit();

    //
    // Semnaleaza threadul consumator ca trebuie sa termine
    // si asteapta
    //
    if (CmdLineGlobalData.MiniFilter.ListenerThreadHandle != NULL)
    {
        SetEvent(CmdLineGlobalData.MiniFilter.ListenerThreadHandleTerminationEvent);
        printf("Wait for the consumer thread to finish...\n");
        WaitForSingleObject(CmdLineGlobalData.MiniFilter.ListenerThreadHandle, INFINITE);
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle != INVALID_HANDLE_VALUE)
    {
        CloseHandle(CmdLineGlobalData.MiniFilter.CommPortHandle);
        CmdLineGlobalData.MiniFilter.CommPortHandle = INVALID_HANDLE_VALUE;
    }

    printf("All cleanup done! Exit now...\n");

    ExitProcess(STATUS_SUCCESS);
}

static
NTSTATUS
CmdPrintAllRunningProcesses(
    DWORD   Argc,
    CHAR    **Argv
)
{
    // This command does not have parameters
    if (Argc != 0 && Argc != 1) { return STATUS_INVALID_PARAMETER; }

    BOOLEAN printCmdLine = FALSE;
    if (Argc == 1)
    {
        if (_stricmp(Argv[0], "true") == 0)
        {
            printCmdLine = TRUE;
        }
        else if (_stricmp(Argv[0], "false") == 0)
        {
            printCmdLine = FALSE;
        }
        else
        {
            printf("Invalid parameters!\n");
            return STATUS_UNSUCCESSFUL;
        }
    }

    // Get the list of process identifiers.
    DWORD aProcesses[1024], cbNeeded, cProcesses;

    if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )
    {
        return STATUS_UNSUCCESSFUL;
    }

    // Calculate how many process identifiers were returned.
    cProcesses = cbNeeded / sizeof(DWORD);

    // Print the name and process identifier for each process.

    for (DWORD i = 0; i < cProcesses; i++ )
    {
        if (!aProcesses[i]) { continue; }

        _PrintProcessNameAndId( aProcesses[i] );
        if (printCmdLine) { _PrintProcessCmdLine(aProcesses[i]); }
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdCallFirstDriverFunction(
    DWORD   Argc,
    CHAR    **Argv
)
{
    UNREFERENCED_PARAMETER(Argc);
    UNREFERENCED_PARAMETER(Argv);

    NTSTATUS status = STATUS_SUCCESS;

    HANDLE hDevice = CreateFile(
        DRIVER_PATH,
        GENERIC_WRITE,
        FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );
    if (hDevice == INVALID_HANDLE_VALUE)
    {
        printf("CreateFile failed! GetLastError = 0x%x\n", GetLastError());
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

    DWORD returned;
    DATA_PACKET dataPacket = { 0 };
    BOOL success = DeviceIoControl(hDevice,
                                   (DWORD)DRIVER_FUNCTION_1,        // control code
                                   &dataPacket, sizeof(dataPacket), // input buffer and length
                                   NULL, 0,                         // output buffer and length
                                   &returned, NULL);
    if (!success)
    {
        printf("DeviceIoControl failed! GetLastError = 0x%x\n", GetLastError());
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

cleanup:
    if (hDevice != INVALID_HANDLE_VALUE) { CloseHandle(hDevice); }

    return status;
}

static
NTSTATUS
CmdCallSecondDriverFunction(
    DWORD   Argc,
    CHAR    **Argv
)
{
    UNREFERENCED_PARAMETER(Argc);
    UNREFERENCED_PARAMETER(Argv);

    NTSTATUS status = STATUS_SUCCESS;

    HANDLE hDevice = CreateFile(
        DRIVER_PATH,
        GENERIC_WRITE,
        FILE_SHARE_WRITE,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );
    if (hDevice == INVALID_HANDLE_VALUE)
    {
        printf("CreateFile failed! GetLastError = 0x%x\n", GetLastError());
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

    DWORD returned;
    DATA_PACKET dataPacket = { 0 };
    BOOL success = DeviceIoControl(hDevice,
                                   (DWORD)DRIVER_FUNCTION_2,        // control code
                                   &dataPacket, sizeof(dataPacket), // input buffer and length
                                   NULL, 0,                         // output buffer and length
                                   &returned, NULL);
    if (!success)
    {
        printf("DeviceIoControl failed! GetLastError = 0x%x\n", GetLastError());
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

cleanup:
    if (hDevice != INVALID_HANDLE_VALUE) { CloseHandle(hDevice); }

    return status;
}

static
NTSTATUS
CmdProtectProcess(
    DWORD   Argc,
    CHAR    **Argv
)
{
    if (Argc != 1)
    {
        printf("Invalid number of arguments!\n");
        return STATUS_UNSUCCESSFUL;
    }

    NTSTATUS status = STATUS_SUCCESS;
    UINT64 processId = strtoll(Argv[0], NULL, 0);

    HANDLE hDevice = CreateFile(
        MYFYLTER_PATH,
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL
    );
    if (hDevice == INVALID_HANDLE_VALUE)
    {
        printf("CreateFile failed! GetLastError = 0x%x\n", GetLastError());
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

    DATA_PACKET dataPacket;
    dataPacket.CommandId = COMMAND_PROTECT_PROCESS_FROM_TERMINATION;
    dataPacket.Parameters.ProtectProcessCmd.ProcessIdToBeProtectedFromKill = processId;

    DWORD returned;
    BOOL success = DeviceIoControl(hDevice,
                                   (DWORD)MY_FILTER_SEND_COMMAND,// control code
                                   &dataPacket, sizeof(dataPacket), // input buffer and length
                                   NULL, 0,                         // output buffer and length
                                   &returned, NULL);
    if (!success)
    {
        printf("DeviceIoControl failed! GetLastError = 0x%x\n", GetLastError());
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

cleanup:
    if (hDevice != INVALID_HANDLE_VALUE) { CloseHandle(hDevice); }

    return status;
}

static
NTSTATUS
CmdPingFsMiniFilter(
    DWORD   Argc,
    CHAR    **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_PING };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    printf("Command PING sent with success. Response = [%s]\n", cmdOutput.Parameters.PingCmd.Output.HelloMsg);
    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStartMonitoringProcesses(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_START_PROCESS_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStopMonitoringProcesses(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_STOP_PROCESS_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStartMonitoringThreads(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_START_THREADS_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStopMonitoringThreads(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_STOP_THREADS_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStartMonitoringLoadImage(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_START_IMAGE_LOAD_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStopMonitoringLoadImage(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_STOP_IMAGE_LOAD_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStartMonitoringRegistry(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_START_REGISTRY_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStopMonitoringRegistry(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_STOP_REGISTRY_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStartMonitoringFiles(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_START_FILES_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdStopMonitoringFiles(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_STOP_FILES_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdMonitorFileDelete(
    DWORD Argc,
    CHAR **Argv
)
{
    UNREFERENCED_PARAMETER(Argv);

    if (Argc != 0)
    {
        printf("No argument required!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    DATA_PACKET cmdInput = { .CommandId = COMMAND_FILE_DELETE_MONITOR };
    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdSetDirectoryProtection(
    DWORD Argc,
    CHAR **Argv
)
{
    if (Argc != 2)
    {
        printf("Wrong number of arguments!\n");
        return STATUS_INVALID_PARAMETER_1;
    }

    if (CmdLineGlobalData.MiniFilter.CommPortHandle == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] Connection with mini filter isn't done!\n");
        return STATUS_UNSUCCESSFUL;
    }

    BOOLEAN enableProtection;
    if (strcmp(Argv[0], "enable") == 0)
    {
        enableProtection = TRUE;
    }
    else if (strcmp(Argv[0], "disable") == 0)
    {
        enableProtection = FALSE;
    }
    else
    {
        printf("Wrong argument!\n");
        return STATUS_INVALID_PARAMETER;
    }

    WCHAR friendlyPath[MAX_SHARED_BUFFER_LENGTH] = { 0 };
    size_t returnVal = 0;
    mbstowcs_s(&returnVal,
        friendlyPath, sizeof(friendlyPath) / sizeof(WORD),
        Argv[1], MAX_SHARED_BUFFER_LENGTH - 1);
    printf("friendlyPath = [%S]\n", friendlyPath);
    printf("friendlyPath + 2 = [%S]\n", friendlyPath + 2);

    DATA_PACKET cmdInput = { .CommandId = COMMAND_SET_PROTECTION_DIRECTORY };
    cmdInput.Parameters.SetProtectionDirectory.Input.EnableProtection = enableProtection;
    wcscpy_s(cmdInput.Parameters.SetProtectionDirectory.Input.DirDosName, MAX_SHARED_BUFFER_LENGTH, friendlyPath + 2);

    DATA_PACKET cmdOutput;

    DWORD bytesWrittenInOutput;
    HRESULT result = FilterSendMessage(
        CmdLineGlobalData.MiniFilter.CommPortHandle,
        (VOID *)&cmdInput, sizeof(cmdInput),
        (VOID *)&cmdOutput, sizeof(cmdOutput),
        &bytesWrittenInOutput
    );
    if (result != S_OK)
    {
        printf("[ERROR] FilterSendMessage failed!!!!\n");
        return STATUS_UNSUCCESSFUL;
    }
    if (!NT_SUCCESS(cmdOutput.Parameters.SetProtectionDirectory.Output.Result))
    {
        printf("ERROR: %S\n", cmdOutput.Parameters.SetProtectionDirectory.Output.ErrorMsg);
    }

    return STATUS_SUCCESS;
}

static
NTSTATUS
CmdBlockSite(
    DWORD Argc,
    CHAR **Argv
)
{
    if (Argc != 1)
    {
        printf("Invalid number of arguments!");
        return STATUS_INVALID_PARAMETER_1;
    }

    CHAR *site = Argv[0];

    return NetworkBlockSite(site);
}

static
NTSTATUS
CmdBlockSiteOnSpecificApp(
    DWORD Argc,
    CHAR **Argv
)
{
    if (Argc != 2)
    {
        printf("Invalid number of arguments!");
        return STATUS_INVALID_PARAMETER_1;
    }

    CHAR *site = Argv[0];
    CHAR *appName = Argv[1];

    return NetworkBlockSiteOnSpecificApp(site, appName);
}