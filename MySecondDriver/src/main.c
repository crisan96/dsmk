#include <ntddk.h>
#include "dispatch_routines.h"
#include "global.h"

/* Unload routine */
VOID MyUnload(_In_ DRIVER_OBJECT *DriverObject);

NTSTATUS
DriverEntry(
    _In_ DRIVER_OBJECT     *DriverObject,
    _In_ UNICODE_STRING    *RegistryPath
)
{
    UNREFERENCED_PARAMETER(RegistryPath);

    //
    // Init global data
    //
    NTSTATUS status = GlobalDataInit();
    if (!NT_SUCCESS(status)) { goto cleanup; }

    //
    // Set unload routine
    //
    DriverObject->DriverUnload = MyUnload;

    //
    // Set dispatch routines
    //
    DriverObject->MajorFunction[IRP_MJ_CREATE] = IrpCreate;
    DriverObject->MajorFunction[IRP_MJ_CLOSE] = IrpClose;
    DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = IrpDeviceControl;

    // Create the device object, so the first
    // driver forward IRPs to us
    UNICODE_STRING deviceName = RTL_CONSTANT_STRING(DRIVER_DEVICE_NAME);
    status = IoCreateDevice(
        DriverObject,                   // our driver object,
        0,                              // no need for extra bytes,
        &deviceName,                    // the device name,
        FILE_DEVICE_UNKNOWN,            // device type,
        0,                              // characteristics flags,
        FALSE,                          // not exclusive,
        &DriverGlobalData->DeviceObject // the resulting pointer
    );
    if (!NT_SUCCESS(status))
    {
        KdPrint(("Failed to create device object (0x%08X)\n", status));
        goto cleanup;
    }

cleanup:
    if (!NT_SUCCESS(status))
    {
        GlobalDataUninit();
    }
    return status;
}

/* Unload routine */
VOID
MyUnload(
    _In_ DRIVER_OBJECT *DriverObject
)
{
    UNREFERENCED_PARAMETER(DriverObject);

    GlobalDataUninit();

    return;
}
