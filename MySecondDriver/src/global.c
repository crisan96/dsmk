#include "global.h"

DRIVER_GLOBAL_DATA *DriverGlobalData;

/**/
NTSTATUS
GlobalDataInit(
    VOID
)
{
    DriverGlobalData = (DRIVER_GLOBAL_DATA *)ExAllocatePoolWithTag(PagedPool, sizeof(DriverGlobalData[0]), DRIVER_TAG);
    if (!DriverGlobalData)
    {
        KdPrint(("ExAllocatePoolWithTag failed!\n"));
        return STATUS_MEMORY_NOT_ALLOCATED;
    }
    memset(DriverGlobalData, 0, sizeof(DriverGlobalData[0]));

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
GlobalDataUninit(
    VOID
)
{
    if (!DriverGlobalData) { return STATUS_SUCCESS; }

    // Sper ca nu crapa functiile de mai jos daca
    // input-ul e "null"

    // Delete device object
    IoDeleteDevice(DriverGlobalData->DeviceObject);

    ExFreePoolWithTag(DriverGlobalData, DRIVER_TAG);
    DriverGlobalData = NULL;

    return STATUS_SUCCESS;
}