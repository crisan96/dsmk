#include "dispatch_routines.h"
#include "common.h"

static NTSTATUS _DriverFunction1(_In_ DEVICE_OBJECT *DeviceObject, _In_ IRP *Irp);
static NTSTATUS _DriverFunction2(_In_ DEVICE_OBJECT *DeviceObject, _In_ IRP *Irp);

/**/
NTSTATUS
IrpCreate(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    UNREFERENCED_PARAMETER(DeviceObject);

    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;

    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
IrpClose(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    UNREFERENCED_PARAMETER(DeviceObject);

    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;

    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
IrpDeviceControl(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    NTSTATUS status = STATUS_SUCCESS;
    IO_STACK_LOCATION *ioStackLocation = IoGetCurrentIrpStackLocation(Irp);

    // Check if the size of the buffer is the expected size
    if (ioStackLocation->Parameters.DeviceIoControl.InputBufferLength != sizeof(DATA_PACKET))
    {
        status = STATUS_BUFFER_TOO_SMALL;
        goto cleanup;
    }

    switch (ioStackLocation->Parameters.DeviceIoControl.IoControlCode)
    {
    case DRIVER_FUNCTION_1:
    {
        status = _DriverFunction1(DeviceObject, Irp);
        goto cleanup;
    }
    case DRIVER_FUNCTION_2:
    {
        status = _DriverFunction2(DeviceObject, Irp);
        goto cleanup;
    }
    default:
    {
        status = STATUS_INVALID_DEVICE_REQUEST;
        break;
    }
    }

cleanup:
    Irp->IoStatus.Status = status;
    Irp->IoStatus.Information = 0;

    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return status;
}

static
NTSTATUS
_DriverFunction1(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    UNREFERENCED_PARAMETER(DeviceObject);
    UNREFERENCED_PARAMETER(Irp);

    KdPrint(("Driver2: Firt IOCTL was called!\n"));

    return STATUS_SUCCESS;
}

static
NTSTATUS
_DriverFunction2(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    UNREFERENCED_PARAMETER(DeviceObject);
    UNREFERENCED_PARAMETER(Irp);

    KdPrint(("Driver2: Second IOCTL was called!\n"));

    return STATUS_SUCCESS;
}