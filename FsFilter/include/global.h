#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <fltKernel.h>
#include "common.h"

/* Driver custom tag for memory allocation */
#define DRIVER_TAG '6BAL'

/*
    Device Name
    The device name could be anything but
    should be in the Device object manager directory.
*/
#define DRIVER_DEVICE_NAME      L"\\Device\\" FS_MINIFILTER_NAME

/* Device symbolic link name */
#define DRIVER_DEVICE_SYM_LINK  L"\\??\\" FS_MINIFILTER_NAME

typedef struct _DRIVER_GLOBAL_DATA
{
    DRIVER_OBJECT   *DriverObject;
    PFLT_FILTER     FilterHandle;
}DRIVER_GLOBAL_DATA;
extern DRIVER_GLOBAL_DATA *DriverGlobalData;

/**/ NTSTATUS GlobalDataInit(DRIVER_OBJECT *DriverObject);
/**/ NTSTATUS GlobalDataUninit(VOID);

#endif // !_GLOBAL_H_
