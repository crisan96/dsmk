#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include <ntdef.h>

NTSTATUS CommInit(VOID);
NTSTATUS CommUninit(VOID);
NTSTATUS CommSendMsgToUserMode(WCHAR *Message);

#endif