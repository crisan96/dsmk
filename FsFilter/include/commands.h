#ifndef _COMMANDS_H_
#define _COMMANDS_H_

#include <ntdef.h>
#include "hooks.h"  // asta ar trebui pus numa in .c (ca sa mentinem
                    // aici numa minimul necesar) DAR NU PREA
                    // MA LASA HEADERELE DE LA MICROSOFT ca dau erori cu redefinitii
#include "common.h"

NTSTATUS CmdInit(VOID);
NTSTATUS CmdUninit(VOID);

//
// Interpretorul de comenzi. Fiecare mesaj trimis din UM ajunge
// aici. In functie de Input se executa comanda (daca este valida)
// si se completeaza Output-ul. Daca status-ul returnat nu este succes,
// atunci output-ul este invalid.
//
NTSTATUS CmdInterpreter(_In_ DATA_PACKET *DataPacketInput, _Out_ DATA_PACKET *DataPacketOutput);

#endif