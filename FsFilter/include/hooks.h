#ifndef _HOOKS_H_
#define _HOOKS_H_

#include <fltKernel.h>

/*
    Entitate ce e wrapper peste functiile de kernel ce inregistreaza
    callback-uri pentru a fi chemate in caz ca apar unele evenimente (creare proces, etc.)
    Mi s-o parut mai usor de citit pentru cineva daca ingradesc cu niste wrappere
*/

//
// Cum acesta este un FileSystem (Mini)Filter hook-urile pe
// lucrul cu fisierele nu se pun dinamic (ma rog, dinamic se pun,
// dar in driver entry cand se apeleaza functia FltRegisterFilter).
// Astfel, avem mai jos niste define-uri: TRUE inseamna ca
// putem avea hook pe operatia respectiva, FALSE inseamna
// ca nu putem avea hook pe operatia respectiva (in spate, in functie
// de aceste define-uri, se pregateste structura ce se da la FltRegisterFilter si alte operatii).
//
#define HOOK_IRP_MJ_CREATE              TRUE
#define HOOK_IRP_MJ_SET_INFORMATION     TRUE
#define HOOK_IRP_MJ_WRITE               TRUE


NTSTATUS HooksInit(_Out_ const FLT_OPERATION_REGISTRATION **CallbacksAvailableForFilter);

// De apelat in drver unload pentru a da jos toate callback-urile setate
// From msdn: A driver must remove any callback routines that it registers before it unloads.
NTSTATUS HooksUninit(VOID);

// Inregistreaza un callback ce sa fie chemat la crearea unui proces
// sau la terminarea unui proces
NTSTATUS HooksAddOnProcessCreationAndProcessExit(PCREATE_PROCESS_NOTIFY_ROUTINE_EX Callback);

// Sterge un hook ce a fost inregistrat in prealabil
// folosind functia HooksAddOnProcessCreationAndProcessExit
NTSTATUS HooksRemoveFromProcessCreationAndProcessExit(PCREATE_PROCESS_NOTIFY_ROUTINE_EX Callback);

// Inregistreaza un callback ce sa fie chemat la crearea unui thread
// sau la terminarea unui thread
NTSTATUS HooksAddOnThreadCreationAndTermination(PCREATE_THREAD_NOTIFY_ROUTINE Callback);

// Sterge un hook ce a fost inregistrat in prealabil
// folosind functia HooksAddOnProcessCreationAndProcessExit
NTSTATUS HooksRemoveFromThreadCreationAndTermination(PCREATE_THREAD_NOTIFY_ROUTINE Callback);

// Hook pe registry keys. Callback-ul atasat va putea
// monitoriza, bloca sau mofifica operatii facute pe registry keys
NTSTATUS HooksAddOnRegistryKeys(PEX_CALLBACK_FUNCTION Callback);

// Sterge un hook ce a fost inregistrat in prealabil
// folosind functia HooksAddOnRegistryKeys.
NTSTATUS HooksRemoveFromRegistryKeys(PEX_CALLBACK_FUNCTION Callback);

// Adauga un hook pe fiecare incarcare a unui modul in memorie
NTSTATUS HooksAddOnImageLoad(PLOAD_IMAGE_NOTIFY_ROUTINE Callback);

// Sterge un hook ce a fost inregistrat in prealabil
// folosind functia HooksAddOnImageLoad.
NTSTATUS HooksRemoveFromImageLoad(PLOAD_IMAGE_NOTIFY_ROUTINE Callback);

// Hook pe handle operations ale proceselor.
// Hook-ul va apela callback-urile atat inainte de operatia pe handle cat si dupa.
NTSTATUS HooksAddOnProcessHandleOperations(POB_PRE_OPERATION_CALLBACK PreOperationCallback, POB_POST_OPERATION_CALLBACK PostOperationCallback);

/************************************************************************************************/
//                      Hook-uri implicite (daca sunt disponibile)                              //
/************************************************************************************************/
NTSTATUS HookSpecificMinifilterIrpCode(UINT32 IrpCode, PFLT_PRE_OPERATION_CALLBACK PreCallback, PFLT_POST_OPERATION_CALLBACK PostCallback); ///< daca a fost pus deja un hook pe acel IRP se va face override
NTSTATUS HookRemoveSpecificMinifilterIrpCode(UINT32 IrpCode);

#endif // !_HOOKS_H_
