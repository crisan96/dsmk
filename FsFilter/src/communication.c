#include "communication.h"
#include <fltKernel.h>
#include "common.h"
#include "global.h"
#include "commands.h"

typedef struct _COMM_GLOBAL_DATA
{
    PFLT_PORT ServerPort;   // used internally to listen to
                            // incoming messages from user mode.

    PFLT_PORT ClientPort;   // handle to the client port which the driver must keep around
                            // and use whenever it needs to communicate with that client.
}COMM_GLOBAL_DATA;
static COMM_GLOBAL_DATA CommGlobalData;

// Callback the driver must provide, called when a new client connects to the port.
static NTSTATUS _CommCoreCallbackConnect(_In_ PFLT_PORT ClientPort, _In_opt_ PVOID ServerPortCookie,
    _In_reads_bytes_opt_(SizeOfContext) PVOID ConnectionContext, _In_ ULONG SizeOfContext,
    _Outptr_result_maybenull_ PVOID *ConnectionPortCookie);

// Callback called when a user mode client disconnects from the port.
static VOID _CommCoreCallbackDisconnect(_In_opt_ PVOID ConnectionCookie);

// The callback invoked when a message arrives on the port.
static NTSTATUS _CommCoreCallbackMessageReceived(_In_opt_ PVOID PortCookie,
    _In_reads_bytes_opt_(InputBufferLength) PVOID InputBuffer, _In_ ULONG InputBufferLength,
    _Out_writes_bytes_to_opt_(OutputBufferLength, *ReturnOutputBufferLength) PVOID OutputBuffer,
    _In_ ULONG OutputBufferLength, _Out_ PULONG ReturnOutputBufferLength);

NTSTATUS
CommInit(
    VOID
)
{
    // Successful call to FltCreateCommunicationPort requires the driver to prepare
    // an object attributes and a security descriptor.The simplest security descriptor
    // can be created with FltBuildDefaultSecurityDescriptor like so :
    SECURITY_DESCRIPTOR *securityDescriptor = NULL;
    NTSTATUS status = FltBuildDefaultSecurityDescriptor(&securityDescriptor, FLT_PORT_ALL_ACCESS);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("FltBuildDefaultSecurityDescriptor failed with status = 0x%x\n", status));
        goto cleanup;
    }

    // The object attributes can then be initialized:
    UNICODE_STRING portName = RTL_CONSTANT_STRING(FS_MINIFILTER_COMM_PORT);
    OBJECT_ATTRIBUTES portAttr;
    InitializeObjectAttributes(&portAttr, &portName,
        OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, securityDescriptor);

    // Create comm port
    status = FltCreateCommunicationPort(
        DriverGlobalData->FilterHandle,
        &CommGlobalData.ServerPort,
        &portAttr,
        NULL,                           // currently not using a communication cookie
        _CommCoreCallbackConnect,
        _CommCoreCallbackDisconnect,
        _CommCoreCallbackMessageReceived,
        1                               // using only one connection
    );
    if (!NT_SUCCESS(status))
    {
        KdPrint(("FltCreateCommunicationPort failed with status = 0x%x\n", status));
        goto cleanup;
    }

cleanup:
    {
        if(securityDescriptor != NULL) FltFreeSecurityDescriptor(securityDescriptor);
    }

    return status;
}

NTSTATUS
CommUninit(
    VOID
)
{
    if (CommGlobalData.ClientPort != NULL)
    {
        FltCloseClientPort(DriverGlobalData->FilterHandle, &CommGlobalData.ClientPort);
        CommGlobalData.ClientPort = NULL;
    }

    if (CommGlobalData.ServerPort != NULL)
    {
        FltCloseCommunicationPort(CommGlobalData.ServerPort);
        CommGlobalData.ServerPort = NULL;
    }

    return STATUS_SUCCESS;
}

NTSTATUS
CommSendMsgToUserMode(
    WCHAR *Message
)
{
    if (Message == NULL) return STATUS_INVALID_PARAMETER;
    if (wcslen(Message) >= MAX_SHARED_BUFFER_LENGTH) return STATUS_INVALID_PARAMETER;
    if (CommGlobalData.ClientPort == NULL) return STATUS_UNSUCCESSFUL;

    DATA_PACKET sendPacket;
    wcscpy(sendPacket.Parameters.ConsumerThread.Message, Message);

    NTSTATUS status = FltSendMessage(
        DriverGlobalData->FilterHandle, &CommGlobalData.ClientPort,
        (VOID *)&sendPacket, sizeof(DATA_PACKET),
        NULL, 0,
        NULL
    );
    if (!NT_SUCCESS(status))
    {
        KdPrint(("failed with status = [0x%x]\n", status));
    }

    return status;
}

// Callback the driver must provide, called when a new client connects to the port.
static
NTSTATUS _CommCoreCallbackConnect(
    _In_ PFLT_PORT  ClientPort,
    _In_opt_ PVOID  ServerPortCookie,
    _In_reads_bytes_opt_(SizeOfContext) PVOID ConnectionContext,
    _In_ ULONG SizeOfContext,
    _Outptr_result_maybenull_ PVOID *ConnectionPortCookie
)
//
// Nu folosim niciun fel de mecanism de login.
// Acceptam conectarea si salvam port-ul clientului.
//
{
    UNREFERENCED_PARAMETER(ServerPortCookie);
    UNREFERENCED_PARAMETER(ConnectionContext); UNREFERENCED_PARAMETER(SizeOfContext);
    UNREFERENCED_PARAMETER(ConnectionPortCookie);

    KdPrint(("%s called!\n", __FUNCTION__));

    CommGlobalData.ClientPort = ClientPort;
    if (ConnectionPortCookie != NULL) *ConnectionPortCookie = NULL;

    return STATUS_SUCCESS;
}

// Callback called when a user mode client disconnects from the port.
static
VOID
_CommCoreCallbackDisconnect(
    _In_opt_ PVOID ConnectionCookie
)
{
    UNREFERENCED_PARAMETER(ConnectionCookie);

    KdPrint(("%s called!\n", __FUNCTION__));

    // When the client disconnects, the _CommCoreCallbackDisconnect callback is invoked.
    // It's important to close the client port at that time,
    // otherwise the mini - filter will never be unloaded.
    FltCloseClientPort(DriverGlobalData->FilterHandle, &CommGlobalData.ClientPort);
    CommGlobalData.ClientPort = NULL;

    return;
}

// The callback invoked when a message arrives on the port.
static
NTSTATUS
_CommCoreCallbackMessageReceived(
    _In_opt_ PVOID PortCookie,
    _In_reads_bytes_opt_(InputBufferLength) PVOID InputBuffer,
    _In_ ULONG InputBufferLength,
    _Out_writes_bytes_to_opt_(OutputBufferLength, *ReturnOutputBufferLength) PVOID OutputBuffer,
    _In_ ULONG OutputBufferLength,
    _Out_ PULONG ReturnOutputBufferLength
)
{
    UNREFERENCED_PARAMETER(PortCookie);

    KdPrint(("%s called!\n", __FUNCTION__));

    if (InputBuffer == NULL || InputBufferLength != sizeof(DATA_PACKET)) return STATUS_INVALID_PARAMETER;
    if (OutputBuffer == NULL || OutputBufferLength != sizeof(DATA_PACKET)) return STATUS_INVALID_PARAMETER;

    DATA_PACKET *dataPacketInput = (DATA_PACKET *)InputBuffer;
    DATA_PACKET *dataPacketOutput = (DATA_PACKET *)OutputBuffer;

    NTSTATUS status = CmdInterpreter(dataPacketInput, dataPacketOutput);
    if (!NT_SUCCESS(status))
    {
        // Ca si o conventie zic sa nu atingem de output decat
        // sa instiintam caller-ul ca nu am returnat niciun byte
        *ReturnOutputBufferLength = 0;
    }
    else
    {
        *ReturnOutputBufferLength = sizeof(DATA_PACKET);
    }

    return status;
}