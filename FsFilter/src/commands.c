/**
 * Comenzile se pot considera a fi exclusive. Mai precis daca
 * doua dintre comenzi au nevoie de acelasi callback pe un FS IRP
 * atunci doar ultima il va avea deoarece callback-urile (pre & post) ce se apeleaza
 * pe un FS IRP se suprascriu. Asta poate fi rezolvat daca in commands.c s-ar pune un callback
 * global si comenzile doar sa activeze un if in acel callback.
 */

#include "commands.h"
#include <ntstrsafe.h>
#include "communication.h"
#include <wdm.h>
#include "global.h"
#include <wchar.h>

/**
 * Variabile folosite pentru protejarea unui director
 */
typedef struct _PROTECTED_DIR_INFO
{
    __declspec(align(32)) volatile LONG AlreadyProtectOneDirectory; // nu folosim mecanisme mai complexe, asumam ca vin sincronizate comenzile din UM
    UNICODE_STRING dirName;
}PROTECTED_DIR_INFO;
static PROTECTED_DIR_INFO ProtectedDir;

/* Callbacks for hooks */
static VOID _CallbackProcessCreationAndProcessExit(_Inout_ PEPROCESS Process,
    _In_ HANDLE ProcessId, _Inout_opt_ PS_CREATE_NOTIFY_INFO *CreateInfo);

static VOID _CallbackThreadsCreationAndTermination(HANDLE ProcessId, HANDLE ThreadId, BOOLEAN Create);

static VOID _CallbackImageLoad(_In_opt_ PUNICODE_STRING FullImageName,
    _In_ HANDLE ProcessId, _In_ PIMAGE_INFO ImageInfo);

static NTSTATUS _CallbackRegistry(_In_ PVOID CallbackContext,
    _In_opt_ PVOID Argument1, _In_opt_ PVOID Argument2);

static FLT_PREOP_CALLBACK_STATUS _CallbackPreOperationIrpMjCreate(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _Flt_CompletionContext_Outptr_ PVOID *CompletionContext);
static FLT_POSTOP_CALLBACK_STATUS _CallbackPostOperationIrpMjCreate(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _In_opt_ PVOID CompletionContext, _In_ FLT_POST_OPERATION_FLAGS Flags);

static FLT_PREOP_CALLBACK_STATUS _DeleteMonitorCallbackPreIrpMjCreate(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _Flt_CompletionContext_Outptr_ PVOID *CompletionContext);
static FLT_POSTOP_CALLBACK_STATUS _DeleteMonitorCallbackPostIrpMjCreate(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _In_opt_ PVOID CompletionContext, _In_ FLT_POST_OPERATION_FLAGS Flags);

static FLT_PREOP_CALLBACK_STATUS _DeleteMonitorCallbackPreIrpMjSetInfo(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _Flt_CompletionContext_Outptr_ PVOID *CompletionContext);
static FLT_POSTOP_CALLBACK_STATUS _DeleteMonitorCallbackPostIrpMjSetInfo(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _In_opt_ PVOID CompletionContext, _In_ FLT_POST_OPERATION_FLAGS Flags);

static FLT_PREOP_CALLBACK_STATUS _ProtectDirCallbackPreIrpMjWrite(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _Flt_CompletionContext_Outptr_ PVOID *CompletionContext);
static FLT_POSTOP_CALLBACK_STATUS _ProtectDirCallbackPostIrpMjWrite(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _In_opt_ PVOID CompletionContext, _In_ FLT_POST_OPERATION_FLAGS Flags);

/* Available commands */
static NTSTATUS _CmdPing(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStartProcessMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStopProcessMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStartThreadsMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStopThreadsMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStartImageLoadMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStopImageLoadMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStartRegistryMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStopRegistryMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStartRegistryMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStopRegistryMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStartFileOperationMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdStopFileOperationMonitor(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdMonitorFileDelete(DATA_PACKET *Input, DATA_PACKET *Output);
static NTSTATUS _CmdSetDirectoryProtection(DATA_PACKET *Input, DATA_PACKET *Output);

/* Static helper functions */
static VOID _PrepareAndSendTextToUserModeApp(_In_opt_ WCHAR *OperationMode, _In_opt_ UINT64 ProcessId,
    _In_opt_ WCHAR *ProcessName, _In_opt_ WCHAR *Path, _In_opt_ WCHAR *Result, _In_opt_ WCHAR *Details);

NTSTATUS
CmdInit(
    VOID
)
{
    return STATUS_SUCCESS;
}

NTSTATUS
CmdUninit(
    VOID
)
{
    if (InterlockedCompareExchange(&ProtectedDir.AlreadyProtectOneDirectory, TRUE, TRUE))
    {
        InterlockedExchange(&ProtectedDir.AlreadyProtectOneDirectory, FALSE);
        RtlFreeUnicodeString(&ProtectedDir.dirName);
    }

    return STATUS_SUCCESS;
}

NTSTATUS
CmdInterpreter(
    _In_ DATA_PACKET *DataPacketInput,
    _Out_ DATA_PACKET *DataPacketOutput
)
//
// Nu mai verificam Input-ul, ar trebui sa fie validat
// deja de callback-ul in care am primit mesajul de la UM
//
{
    switch (DataPacketInput->CommandId)
    {
    case COMMAND_PING:  return _CmdPing(DataPacketInput, DataPacketOutput);

    case COMMAND_START_PROCESS_MONITOR: return _CmdStartProcessMonitor(DataPacketInput, DataPacketOutput);
    case COMMAND_STOP_PROCESS_MONITOR: return _CmdStopProcessMonitor(DataPacketInput, DataPacketOutput);

    case COMMAND_START_THREADS_MONITOR: return _CmdStartThreadsMonitor(DataPacketInput, DataPacketOutput);
    case COMMAND_STOP_THREADS_MONITOR: return _CmdStopThreadsMonitor(DataPacketInput, DataPacketOutput);

    case COMMAND_START_IMAGE_LOAD_MONITOR: return _CmdStartImageLoadMonitor(DataPacketInput, DataPacketOutput);
    case COMMAND_STOP_IMAGE_LOAD_MONITOR: return _CmdStopImageLoadMonitor(DataPacketInput, DataPacketOutput);

    case COMMAND_START_REGISTRY_MONITOR: return _CmdStartRegistryMonitor(DataPacketInput, DataPacketOutput);
    case COMMAND_STOP_REGISTRY_MONITOR: return _CmdStopRegistryMonitor(DataPacketInput, DataPacketOutput);

    case COMMAND_START_FILES_MONITOR: return _CmdStartFileOperationMonitor(DataPacketInput, DataPacketOutput);
    case COMMAND_STOP_FILES_MONITOR: return _CmdStopFileOperationMonitor(DataPacketInput, DataPacketOutput);

    case COMMAND_FILE_DELETE_MONITOR: return _CmdMonitorFileDelete(DataPacketInput, DataPacketOutput);

    case COMMAND_SET_PROTECTION_DIRECTORY: return _CmdSetDirectoryProtection(DataPacketInput, DataPacketOutput);

    default: return STATUS_NOT_IMPLEMENTED;
    }
}

static
VOID
_PrepareAndSendTextToUserModeApp(
    _In_opt_    WCHAR   *OperationMode,
    _In_opt_    UINT64   ProcessId,
    _In_opt_    WCHAR   *ProcessName,
    _In_opt_    WCHAR   *Path,
    _In_opt_    WCHAR   *Result,
    _In_opt_    WCHAR   *Details
)
{

    NTSTRSAFE_PWSTR msg = ExAllocatePoolWithTag(PagedPool, MAX_SHARED_BUFFER_LENGTH * sizeof(WCHAR), DRIVER_TAG);
    if (msg == NULL)
    {
        KdPrint(("ExAllocatePoolWithTag failed\n"));
        return;
    }

    LARGE_INTEGER timestamp = { 0 };
    KeQuerySystemTime(&timestamp);

    NTSTATUS status = RtlStringCbPrintfW(msg, MAX_SHARED_BUFFER_LENGTH * sizeof(WCHAR),
        L"[%llu][%s][%llu][%s][%s][%s][%s]",
        timestamp.QuadPart,
        OperationMode != NULL ? OperationMode : L"N/A",
        ProcessId != 0 ? ProcessId : -1,
        ProcessName != NULL ? ProcessName : L"N/A",
        Path != NULL ? Path : L"N/A",
        Result != NULL ? Result : L"N/A",
        Details != NULL ? Details : L"N/A"
    );
    if (NT_SUCCESS(status))
    {
        //KdPrint(("%S\n", msg));
        NTSTATUS localStatus = CommSendMsgToUserMode(msg);
        if (!NT_SUCCESS(localStatus))
        {
            KdPrint(("CommSendMsgToUserMode failed with status = [0x%x]\n", localStatus));
        }
    }
    else
    {
        KdPrint(("RtlUnicodeStringPrintf failed with status = (0x%X)\n", status));
    }

    if (msg != NULL) ExFreePoolWithTag(msg, DRIVER_TAG);

    return;
}

/******************************************/
//          Callbacks for hooks           //
/****************************************/
static
VOID
_CallbackProcessCreationAndProcessExit(
    _Inout_     PEPROCESS               Process,
    _In_        HANDLE                  ProcessId,
    _Inout_opt_ PS_CREATE_NOTIFY_INFO *CreateInfo
)
{
    /// !!!TODO: Implementare proasta numa sa vad ca merge

    UNREFERENCED_PARAMETER(Process);

    LARGE_INTEGER timestamp = { 0 };
    KeQuerySystemTime(&timestamp);

    BOOLEAN processCreate = (CreateInfo != NULL) ? TRUE : FALSE;
    BOOLEAN hasProcessName = (CreateInfo != NULL && !CreateInfo->IsSubsystemProcess) ? TRUE : FALSE;
    BOOLEAN hasCmdLine = (CreateInfo != NULL && CreateInfo->CommandLine != NULL) ? TRUE : FALSE;

    _PrepareAndSendTextToUserModeApp(
        processCreate ? L"Process Create" : L"Process Terminate",
        (UINT64)ProcessId,
        hasProcessName ? CreateInfo->ImageFileName->Buffer : NULL,
        NULL,
        NULL,
        hasCmdLine ? CreateInfo->CommandLine->Buffer : NULL
    );

    return;
}

static
VOID
_CallbackThreadsCreationAndTermination(
    HANDLE ProcessId,
    HANDLE ThreadId,
    BOOLEAN Create
)
{
    WCHAR details[32] = { 0 };
    RtlStringCbPrintfW(details, 32, L"Thread Id = %llu", (UINT64)ThreadId);

    _PrepareAndSendTextToUserModeApp(
        Create ? L"Thread CREATE" : L"Thread TERMINATE",
        (UINT64)ProcessId,
        NULL,
        NULL,
        NULL,
        details[0] != 0 ? details : NULL
    );

    return;
}

static
VOID
_CallbackImageLoad(
    _In_opt_ PUNICODE_STRING FullImageName,
    _In_ HANDLE ProcessId,
    _In_ PIMAGE_INFO ImageInfo
)
{
    UNREFERENCED_PARAMETER(ImageInfo);

    _PrepareAndSendTextToUserModeApp(
        L"Image LOAD",
        (UINT64)ProcessId,
        NULL,
        FullImageName != NULL ? FullImageName->Buffer : NULL,
        NULL,
        NULL
    );

    return;
}

static
NTSTATUS
_CallbackRegistry(
    _In_        PVOID CallbackContext,
    _In_opt_    PVOID Argument1,
    _In_opt_    PVOID Argument2
)
{
    UNREFERENCED_PARAMETER(CallbackContext);

    if (Argument1 == NULL) return STATUS_INVALID_PARAMETER_2;
    if (Argument2 == NULL) return STATUS_INVALID_PARAMETER_3;

    // The callback routine must enclose any access
    // of an output buffer in a try/except block
    SIZE_T regNotifyClass = (SIZE_T)Argument1;
    __try
    {
        switch (regNotifyClass)
        {
        case RegNtPreRenameKey:
        {
            REG_RENAME_KEY_INFORMATION *info = (REG_RENAME_KEY_INFORMATION *)Argument2;
            _PrepareAndSendTextToUserModeApp(
                L"Reg Key RENAMED",
                0,
                NULL,
                NULL,
                NULL,
                info->NewName->Buffer
            );
            break;
        }

        default:
        {
            _PrepareAndSendTextToUserModeApp(
                L"Reg Key OPERATION",
                0,
                NULL,
                NULL,
                NULL,
                L"UNKNOWN or NOT IMPLEMENTED Operation"
            );
            break;
        }
        }
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
        return STATUS_UNSUCCESSFUL;
    }

    return STATUS_SUCCESS;
}

static
FLT_PREOP_CALLBACK_STATUS
_CallbackPreOperationIrpMjCreate(
    _Inout_ PFLT_CALLBACK_DATA              Data,
    _In_    PCFLT_RELATED_OBJECTS           FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
    UNREFERENCED_PARAMETER(Data); UNREFERENCED_PARAMETER(FltObjects); UNREFERENCED_PARAMETER(CompletionContext);
    KdPrint(("!!!! %s called!!!\n", __FUNCTION__));
    return FLT_PREOP_SUCCESS_WITH_CALLBACK;
}

static
FLT_POSTOP_CALLBACK_STATUS
_CallbackPostOperationIrpMjCreate(
    _Inout_ PFLT_CALLBACK_DATA  Data,
    _In_ PCFLT_RELATED_OBJECTS  FltObjects,
    _In_opt_ PVOID              CompletionContext,
    _In_ FLT_POST_OPERATION_FLAGS Flags
)
{
    UNREFERENCED_PARAMETER(Data); UNREFERENCED_PARAMETER(FltObjects); UNREFERENCED_PARAMETER(CompletionContext);
    UNREFERENCED_PARAMETER(Flags);
    KdPrint(("!!!! %s called!!!\n", __FUNCTION__));
    return FLT_POSTOP_FINISHED_PROCESSING;
}

static
FLT_PREOP_CALLBACK_STATUS
_DeleteMonitorCallbackPreIrpMjCreate(
    _Inout_ PFLT_CALLBACK_DATA              Data,
    _In_    PCFLT_RELATED_OBJECTS           FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID    *CompletionContext
)
{
    UNREFERENCED_PARAMETER(CompletionContext); UNREFERENCED_PARAMETER(FltObjects);
    // First we'll check if the operation is originating from kernel mode,
    // and if so, just let it continue uninterrupted
    if (Data->RequestorMode == KernelMode) return FLT_PREOP_SUCCESS_NO_CALLBACK;

    // Next we need to check if the flag FILE_DELETE_ON_CLOSE exists in the creation request.The
    // structure to look at is the Create field under the Paramaters inside Iopb like so
    // Deci daca nu e delete putem sa returnam succes si ca nu vrem callback pe post.
    if (!(Data->Iopb->Parameters.Create.Options & FILE_DELETE_ON_CLOSE)) return FLT_PREOP_SUCCESS_NO_CALLBACK;

    // Deci, e delete.
    _PrepareAndSendTextToUserModeApp(
        L"File Deleted",
        0,
        NULL,
        Data->Iopb->TargetFileObject->FileName.Buffer,
        NULL,
        NULL
    );

    return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

static
FLT_POSTOP_CALLBACK_STATUS
_DeleteMonitorCallbackPostIrpMjCreate(
    _Inout_ PFLT_CALLBACK_DATA  Data,
    _In_ PCFLT_RELATED_OBJECTS  FltObjects,
    _In_opt_ PVOID              CompletionContext,
    _In_ FLT_POST_OPERATION_FLAGS Flags
)
{
    UNREFERENCED_PARAMETER(Data); UNREFERENCED_PARAMETER(FltObjects); UNREFERENCED_PARAMETER(CompletionContext);
    UNREFERENCED_PARAMETER(Flags);
    KdPrint(("%s called\n", __FUNCTION__));
    return FLT_POSTOP_FINISHED_PROCESSING;
}

static
FLT_PREOP_CALLBACK_STATUS
_DeleteMonitorCallbackPreIrpMjSetInfo(
    _Inout_ PFLT_CALLBACK_DATA              Data,
    _In_    PCFLT_RELATED_OBJECTS           FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
    UNREFERENCED_PARAMETER(CompletionContext); UNREFERENCED_PARAMETER(FltObjects);
    // First we'll check if the operation is originating from kernel mode,
    // and if so, just let it continue uninterrupted
    if (Data->RequestorMode == KernelMode) return FLT_PREOP_SUCCESS_NO_CALLBACK;

    // FileInformationClass indicates which type of operation this instance represents and so we need
    // to check whether this is a delete operation.
    // The FileDispositionInformation enumeration value indicates a delete operation.
    if (
        (Data->Iopb->Parameters.SetFileInformation.FileInformationClass != FileDispositionInformation)
        && (Data->Iopb->Parameters.SetFileInformation.FileInformationClass != FileDispositionInformationEx)
        )
    {
        // not a delete operation
        return FLT_PREOP_SUCCESS_NO_CALLBACK;
    }

    // If it is a delete operation, there is yet another check to do, but looking at the information buffer
    // which is of type FILE_DISPOSITION_INFORMATION for delete operations andchecking the boolean stored there.
    FILE_DISPOSITION_INFORMATION *info = (FILE_DISPOSITION_INFORMATION *)Data->Iopb->Parameters.SetFileInformation.InfoBuffer;
    if (!info->DeleteFile) return FLT_PREOP_SUCCESS_NO_CALLBACK;

    // Deci, e delete.
    _PrepareAndSendTextToUserModeApp(
        L"File Deleted",
        0,
        NULL,
        Data->Iopb->TargetFileObject->FileName.Buffer,
        NULL,
        NULL
    );

    return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

static
FLT_POSTOP_CALLBACK_STATUS
_DeleteMonitorCallbackPostIrpMjSetInfo(
    _Inout_ PFLT_CALLBACK_DATA  Data,
    _In_ PCFLT_RELATED_OBJECTS  FltObjects,
    _In_opt_ PVOID              CompletionContext,
    _In_ FLT_POST_OPERATION_FLAGS Flags
)
{
    UNREFERENCED_PARAMETER(Data); UNREFERENCED_PARAMETER(FltObjects); UNREFERENCED_PARAMETER(CompletionContext);
    UNREFERENCED_PARAMETER(Flags);
    KdPrint(("%s called\n", __FUNCTION__));
    return FLT_POSTOP_FINISHED_PROCESSING;
}

static
FLT_PREOP_CALLBACK_STATUS
_ProtectDirCallbackPreIrpMjWrite(
    _Inout_ PFLT_CALLBACK_DATA              Data,
    _In_    PCFLT_RELATED_OBJECTS           FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
    UNREFERENCED_PARAMETER(FltObjects); UNREFERENCED_PARAMETER(CompletionContext);

    // Daca nu avem director protejat..
    if (!InterlockedCompareExchange(&ProtectedDir.AlreadyProtectOneDirectory, TRUE, TRUE)) return FLT_PREOP_SUCCESS_NO_CALLBACK;

    // Departe de a fi o implementare buna dar merge cel putin in cazul testat
    BOOLEAN fileInProtectedDirectory = TRUE;
    for (UINT32 i = 0; i < ProtectedDir.dirName.Length / 2U; ++i)
    {
        if (i >= Data->Iopb->TargetFileObject->FileName.Length)
        {
            fileInProtectedDirectory = FALSE;
            break;
        }
        if(Data->Iopb->TargetFileObject->FileName.Buffer[i] != ProtectedDir.dirName.Buffer[i])
        {
            fileInProtectedDirectory = FALSE;
            break;
        }
    }

    if(fileInProtectedDirectory)
    {
        KdPrint(("!!! BLOCK WRITE TO FILE = [%S]\n", Data->Iopb->TargetFileObject->FileName.Buffer));
        Data->IoStatus.Status = STATUS_ACCESS_DENIED;
        return FLT_PREOP_COMPLETE;
    }

    return FLT_PREOP_SUCCESS_NO_CALLBACK;
}

static
FLT_POSTOP_CALLBACK_STATUS
_ProtectDirCallbackPostIrpMjWrite(
    _Inout_ PFLT_CALLBACK_DATA  Data,
    _In_ PCFLT_RELATED_OBJECTS  FltObjects,
    _In_opt_ PVOID              CompletionContext,
    _In_ FLT_POST_OPERATION_FLAGS Flags
)
{
    UNREFERENCED_PARAMETER(Data); UNREFERENCED_PARAMETER(FltObjects); UNREFERENCED_PARAMETER(CompletionContext);
    UNREFERENCED_PARAMETER(Flags);
    KdPrint(("%s called\n", __FUNCTION__));
    return FLT_POSTOP_FINISHED_PROCESSING;
}

/**********************/
/* Available commands */
/**********************/
static
NTSTATUS
_CmdPing(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input);

    KdPrint(("!!! PING from UserMode !!!\n"));

    memcpy(Output->Parameters.PingCmd.Output.HelloMsg, "Hello from MiniFilter!", sizeof("Hello from MiniFilter!"));

    return STATUS_SUCCESS;
}

static
NTSTATUS
_CmdStartProcessMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    // Chiar daca user-ul apeleaza de 2 ori comanda start process
    // API-ul de adaugat hook va faila pt ca deja exista acest hook pus
    NTSTATUS status = HooksAddOnProcessCreationAndProcessExit(_CallbackProcessCreationAndProcessExit);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnProcessCreationAndProcessExit failed with status = (0x%X)\n", status));
    }

    return status;
}

static
NTSTATUS
_CmdStopProcessMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    NTSTATUS status = HooksRemoveFromProcessCreationAndProcessExit(_CallbackProcessCreationAndProcessExit);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksRemoveFromProcessCreationAndProcessExit failed with status = (0x%X)\n", status));
    }

    return status;
}

static
NTSTATUS
_CmdStartThreadsMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    // Chiar daca user-ul apeleaza de 2 ori comanda start thread monitor
    // API-ul de adaugat hook va faila pt ca deja exista acest hook pus
    NTSTATUS status = HooksAddOnThreadCreationAndTermination(_CallbackThreadsCreationAndTermination);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnThreadCreationAndTermination failed with status = (0x%X)\n", status));
    }

    return status;
}

static
NTSTATUS
_CmdStopThreadsMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    NTSTATUS status = HooksRemoveFromThreadCreationAndTermination(_CallbackThreadsCreationAndTermination);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksRemoveFromThreadCreationAndTermination failed with status = (0x%X)\n", status));
    }

    return status;
}

static
NTSTATUS
_CmdStartImageLoadMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    // Chiar daca user-ul apeleaza de 2 ori comanda start load monitor
    // API-ul de adaugat hook va faila pt ca deja exista acest hook pus
    NTSTATUS status = HooksAddOnImageLoad(_CallbackImageLoad);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnImageLoad failed with status = (0x%X)\n", status));
    }

    return status;
}

static
NTSTATUS
_CmdStopImageLoadMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    NTSTATUS status = HooksRemoveFromImageLoad(_CallbackImageLoad);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksRemoveFromImageLoad failed with status = (0x%X)\n", status));
    }

    return status;
}

static
NTSTATUS
_CmdStartRegistryMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    // Chiar daca user-ul apeleaza de 2 ori comanda start load registry monitor
    // API-ul de adaugat hook va faila pt ca deja exista acest hook pus
    NTSTATUS status = HooksAddOnRegistryKeys(_CallbackRegistry);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnRegistryKeys failed with status = (0x%X)\n", status));
    }

    return status;
}

static
NTSTATUS
_CmdStopRegistryMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    NTSTATUS status = HooksRemoveFromRegistryKeys(_CallbackRegistry);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksRemoveFromRegistryKeys failed with status = (0x%X)\n", status));
    }

    return status;
}

typedef struct _IRP_HOOK
{
    UINT32                          IrpCode;
    PFLT_PRE_OPERATION_CALLBACK     PreCallback;
    PFLT_POST_OPERATION_CALLBACK    PostCallback;
}IRP_HOOK;
static IRP_HOOK IrpHooks[] =
{
    {IRP_MJ_CREATE, _CallbackPreOperationIrpMjCreate, _CallbackPostOperationIrpMjCreate},
};
#define ARRAY_HOOKS_LENGTH ARRAYSIZE(IrpHooks)

static
NTSTATUS
_CmdStartFileOperationMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    BOOLEAN atLeastOneHookFailed = FALSE;

    for (UINT32 i = 0; i < ARRAY_HOOKS_LENGTH; ++i)
    {
        NTSTATUS status = HookSpecificMinifilterIrpCode(
            IrpHooks[i].IrpCode,
            IrpHooks[i].PreCallback,
            IrpHooks[i].PostCallback
        );
        if (!NT_SUCCESS(status))
        {
            atLeastOneHookFailed = TRUE;
            KdPrint(("HooksAddOnRegistryKeys failed with status = (0x%X)\n", status));
        }
    }

    return (atLeastOneHookFailed ? STATUS_UNSUCCESSFUL : STATUS_SUCCESS);
}

static
NTSTATUS
_CmdStopFileOperationMonitor(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    for (UINT32 i = 0; i < ARRAY_HOOKS_LENGTH; ++i) HookRemoveSpecificMinifilterIrpCode(IrpHooks[i].IrpCode);

    return STATUS_SUCCESS;
}

static
NTSTATUS
_CmdMonitorFileDelete(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    UNREFERENCED_PARAMETER(Input); UNREFERENCED_PARAMETER(Output);

    // It turns out there are two way to delete a file. One way is to use IRP_MJ_SET_INFORMATION
    // operation.This operation provides a bag of operations, delete just being one of them.The second way
    // to delete a file(and in fact the most common) is to open the file with the FILE_DELETE_ON_CLOSE
    // option flag.The file then is deleted as soon as that last handle to it is closed.
    // ==> trebuie sa punem hook pe IRP_MJ_CREATE si HOOK_IRP_MJ_SET_INFORMATION
    NTSTATUS status = HookSpecificMinifilterIrpCode(IRP_MJ_CREATE,
        _DeleteMonitorCallbackPreIrpMjCreate, _DeleteMonitorCallbackPostIrpMjCreate);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HookSpecificMinifilterIrpCode failed with status = (0x%X)\n", status));
        return status;
    }

    status = HookSpecificMinifilterIrpCode(IRP_MJ_SET_INFORMATION,
        _DeleteMonitorCallbackPreIrpMjSetInfo, _DeleteMonitorCallbackPostIrpMjSetInfo);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HookSpecificMinifilterIrpCode failed with status = (0x%X)\n", status));
        return status;
    }

    return status;
}

static
NTSTATUS
_CmdSetDirectoryProtection(
    DATA_PACKET *Input,
    DATA_PACKET *Output
)
{
    // Fara multe (sau deloc) input checks
    BOOLEAN needToFreeTheName = FALSE;

    if (
        (Input->Parameters.SetProtectionDirectory.Input.EnableProtection)
        && InterlockedCompareExchange(&ProtectedDir.AlreadyProtectOneDirectory, TRUE, TRUE)
        )
    {
        Output->Parameters.SetProtectionDirectory.Output.Result = STATUS_ALREADY_INITIALIZED;
        RtlCopyMemory(Output->Parameters.SetProtectionDirectory.Output.ErrorMsg,
            L"A directory is already protected. Remove the initial protection first",
            sizeof(L"A directory is already protected. Remove the initial protection first")
        );

        return STATUS_SUCCESS;
    }

    UNICODE_STRING dirName;
    RtlCreateUnicodeString(&dirName, Input->Parameters.SetProtectionDirectory.Input.DirDosName);
    if (!Input->Parameters.SetProtectionDirectory.Input.EnableProtection)
    {
        // Daca nu avem protejat nimic
        if (!InterlockedCompareExchange(&ProtectedDir.AlreadyProtectOneDirectory, FALSE, FALSE))
        {
            Output->Parameters.SetProtectionDirectory.Output.Result = STATUS_SUCCESS;
            return STATUS_SUCCESS;
        }
        else
        {
            // Daca avem protejat ceva, sa vedem daca numele coincide sau nu.
            // Daca nu, eroare.
            if (RtlCompareUnicodeString(&dirName, &ProtectedDir.dirName, TRUE) != 0)
            {
                Output->Parameters.SetProtectionDirectory.Output.Result = STATUS_ALREADY_INITIALIZED;
                RtlCopyMemory(Output->Parameters.SetProtectionDirectory.Output.ErrorMsg,
                    L"You want to remove the protection from a folder that was not protected",
                    sizeof(L"you want to remove the protection from a folder that was not protected")
                );
                needToFreeTheName = TRUE;

                goto cleanup;
            }
        }
    }

    if (Input->Parameters.SetProtectionDirectory.Input.EnableProtection)
    {
        KdPrint(("[%s] Dir name = %S\n!", __FUNCTION__, dirName.Buffer));
        Output->Parameters.SetProtectionDirectory.Output.Result = STATUS_SUCCESS;
        ProtectedDir.dirName = dirName;
        InterlockedExchange(&ProtectedDir.AlreadyProtectOneDirectory, TRUE);

        // Trebuie sa interceptam IRP_MJ_WRITE
        NTSTATUS status = HookSpecificMinifilterIrpCode(IRP_MJ_WRITE,
            _ProtectDirCallbackPreIrpMjWrite, _ProtectDirCallbackPostIrpMjWrite);
        if (!NT_SUCCESS(status))
        {
            KdPrint(("HookSpecificMinifilterIrpCode failed with status = (0x%X)\n", status));
            return status;
        }
    }
    else
    {
        InterlockedExchange(&ProtectedDir.AlreadyProtectOneDirectory, FALSE);
        RtlFreeUnicodeString(&ProtectedDir.dirName);
        needToFreeTheName = TRUE;

        NTSTATUS status = HookRemoveSpecificMinifilterIrpCode(IRP_MJ_WRITE);
        if (!NT_SUCCESS(status))
        {
            KdPrint(("HookRemoveSpecificMinifilterIrpCode failed with status = (0x%X)\n", status));
            return status;
        }
    }

    Output->Parameters.SetProtectionDirectory.Output.Result = STATUS_SUCCESS;

cleanup:
    if (needToFreeTheName) RtlFreeUnicodeString(&dirName);

    return STATUS_SUCCESS;
}