#include "hooks.h"
#include <ntddk.h>
#include "global.h"

//
// Descrierea ce fel de hook este
//
typedef enum _HOOK_TYPE
{
    HOOK_TYPE_PROCESS_CREATE_PROCESS_EXIT,  // Hook-ul acesta va fi adaugat de catre HooksAddOnProcessCreationAndProcessExit,
                                            // si poate fi stert de catre HooksAddOnProcessCreationAndProcessExit

    HOOK_TYPE_THREAD_CREATE_THREAD_TERMINATE,

    HOOK_TYPE_REGISTRY_KEYS,                // Hook pe registry keys

    HOOK_TYPE_IMAGE_LOAD,                   // Hook pe fiecare incarcare in memorie a unui modul

    HOOK_TYPE_OB,                           // Hook ce se activeaza cu functia ObRegisterCallbacks
}HOOK_TYPE;

#pragma warning(push)
#pragma warning(disable:4201)
// La inregistrare, functiile returneaza (sau nu, caz in care ID-ul este
// un input dat functiei) un oarecare ID.
// Acest ID trebuie salvat deoarece este folosit impreuna cu functia ce face
// revert la inregistrare.
// Long story short: In functie de campul HookType aici ar trebui sa gasim
// un Id specific cu care sa apelam functia de revert la inregistrare.
typedef struct _HOOK_ID
{
    VOID *CallbackAddress;      // Folosim oarecum ca si o cheie. Consider
                                // ca ar fi putin ciudat sa folosesti aceasi functie
                                // de 2 ori pentru un hook. (in cazul hook-urilor activate
                                // cu functia ObRegisterCallbacks, aici se afla doar PreCallback)
                                // Uneori este chiar ID ce se foloseste la functia de unregister.

    union
    {
        LARGE_INTEGER   Registry;   // Este ID-ul hook-ului pe registry keys.
                                    // Cu ajutorul lui putem scoate hook-ul
                                    // (pe MSDN se gaseste sub numele 'Cookie')

        VOID            *Ob;        // Id-ul hook-ului pus de functia ObRegisterCallbacks
                                    // Cu ajutorul lui putem scoate hook-ul
    };
}HOOK_ID;
#pragma warning(pop)

typedef struct _HOOK
{
    LIST_ENTRY      ListEntry;

    // Tipul hook-ului (pe chei de registrii, pe creare/terminare proces, etc...)
    HOOK_TYPE       HookType;

    // La inregistrare, functiile returneaza (sau nu, caz in care ID-ul este
    // un input dat functiei) un oarecare ID.
    // Acest ID trebuie salvat deoarece este folosit impreuna cu functia ce face
    // revert la inregistrare.
    // Long story short: In functie de campul HookType aici ar trebui sa gasim
    // un Id specific cu care sa apelam functia de revert la inregistrare.
    HOOK_ID         HookId;
}HOOK;

//
// Ce operatii hook-am implicit avand in vedere ca
// acesta este un FS MiniFilter
//
#if(HOOK_IRP_MJ_CREATE)
static FLT_PREOP_CALLBACK_STATUS _MiniFilterPreOperationIrpMjCreate(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _Flt_CompletionContext_Outptr_ PVOID *CompletionContext);
static FLT_POSTOP_CALLBACK_STATUS _MiniFilterPostOperationIrpMjCreate(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _In_opt_ PVOID CompletionContext, _In_ FLT_POST_OPERATION_FLAGS Flags);
#endif
#if(HOOK_IRP_MJ_SET_INFORMATION)
static FLT_PREOP_CALLBACK_STATUS _MiniFilterPreOperationIrpMjSetInfo(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _Flt_CompletionContext_Outptr_ PVOID *CompletionContext);
static FLT_POSTOP_CALLBACK_STATUS _MiniFilterPostOperationIrpMjSetInfo(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _In_opt_ PVOID CompletionContext, _In_ FLT_POST_OPERATION_FLAGS Flags);
#endif
#if(HOOK_IRP_MJ_WRITE)
static FLT_PREOP_CALLBACK_STATUS _MiniFilterPreOperationIrpMjWrite(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _Flt_CompletionContext_Outptr_ PVOID *CompletionContext);
static FLT_POSTOP_CALLBACK_STATUS _MiniFilterPostOperationIrpMjWrite(_Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects, _In_opt_ PVOID CompletionContext, _In_ FLT_POST_OPERATION_FLAGS Flags);
#endif

static const FLT_OPERATION_REGISTRATION AvailableCallbacks[] =
{
#if(HOOK_IRP_MJ_CREATE)
    {
    .MajorFunction = IRP_MJ_CREATE, .Flags = 0,
    .PreOperation = _MiniFilterPreOperationIrpMjCreate,
    .PostOperation = _MiniFilterPostOperationIrpMjCreate
    },
#endif
#if(HOOK_IRP_MJ_SET_INFORMATION)
    {
    .MajorFunction = IRP_MJ_SET_INFORMATION, .Flags = 0,
    .PreOperation = _MiniFilterPreOperationIrpMjSetInfo,
    .PostOperation = _MiniFilterPostOperationIrpMjSetInfo
    },
#endif
#if(HOOK_IRP_MJ_WRITE)
    {
    .MajorFunction = IRP_MJ_WRITE, .Flags = 0,
    .PreOperation = _MiniFilterPreOperationIrpMjWrite,
    .PostOperation = _MiniFilterPostOperationIrpMjWrite
    },
#endif
    { IRP_MJ_OPERATION_END }
};

typedef struct _HOOKS_GLOBAL_DATA
{
    BOOLEAN IsComponentInited;

    struct
    {
        // Mai multe structuri specifice in care user-ul
        // isi va inregistra callback-urile pentru hook-urile
        // implicite cand va dori. De exemplu, IRP_MJ_CREATE
        // este setat ca implicit sa primim dar functia callback nu va
        // face decat sa "returneze success" pana cand user-ul
        // nu va vrea chiar sa primeasca informatii, caz in care
        // isi va inregistra un callback ce va fi apelat din functia callback.
        // O incercare mai urata de a pastra toata logica in fisierul asta...
#if(HOOK_IRP_MJ_CREATE)
        struct
        {
            __declspec(align(32)) volatile LONG AreCallbacksAvailable;
            PFLT_PRE_OPERATION_CALLBACK         PreCallback;
            PFLT_POST_OPERATION_CALLBACK        PostCallback;
        }IrpMjCreate;   // Obs: toate structurile o sa arate la fel deci probabil
                        //  era mai frumos sa fie o structura de date ce sa aiba cheia IRP_...
#endif
#if(HOOK_IRP_MJ_SET_INFORMATION)
        struct
        {
            __declspec(align(32)) volatile LONG AreCallbacksAvailable;
            PFLT_PRE_OPERATION_CALLBACK         PreCallback;
            PFLT_POST_OPERATION_CALLBACK        PostCallback;
        }IrpMjSetInformation;
#endif
#if(HOOK_IRP_MJ_SET_INFORMATION)
        struct
        {
            __declspec(align(32)) volatile LONG AreCallbacksAvailable;
            PFLT_PRE_OPERATION_CALLBACK         PreCallback;
            PFLT_POST_OPERATION_CALLBACK        PostCallback;
        }IrpMjWrite;
#endif

        UINT8 Dummy;
    }MiniFilterSpecificHooks;

    struct
    {
        UINT64      NumberOfHooks;

        // Nota: nu folosim niciun soi de sincronizare
        // pentru manipularea listei de hook-uri deoarece nu ar
        // trebui sa avem nici concurenta nici paralelism!!
        LIST_ENTRY  ListHead;    // O lista de pointeri la callback-urile inregistrare.
                                 // Folositoare pentru uninit, ca sa stim ce hook-uri dam jos
    }Hooks;
}HOOKS_GLOBAL_DATA;
static HOOKS_GLOBAL_DATA HooksGlobalData;

/* Static functions */
static LIST_ENTRY*  _ReturnHookListEntry(HOOK_TYPE HookType, HOOK_ID *HookId);
static NTSTATUS     _AddNewHookInList(HOOK_TYPE HookType, HOOK_ID *HookId);
static NTSTATUS     _RemoveHookFromList(HOOK_TYPE HookType, HOOK_ID *HookId);
static VOID         _HookDisableFromKernel(HOOK_TYPE HookType, HOOK_ID *HookId);

NTSTATUS
HooksInit(
    _Out_ const FLT_OPERATION_REGISTRATION **CallbacksAvailableForFilter
)
{
    if (CallbacksAvailableForFilter == NULL) return STATUS_INVALID_PARAMETER;

    //
    // Initializeaza lista de hook-uri
    //
    InitializeListHead(&HooksGlobalData.Hooks.ListHead);

    HooksGlobalData.IsComponentInited = TRUE;

    *CallbacksAvailableForFilter = AvailableCallbacks;

    return STATUS_SUCCESS;
}

NTSTATUS
HooksUninit(
    VOID
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_SUCCESS; }

    while (TRUE)
    {
        LIST_ENTRY *listEntry = RemoveHeadList(&HooksGlobalData.Hooks.ListHead);

        // If the list is empty, RemoveHeadList returns ListHead.
        if (listEntry == &HooksGlobalData.Hooks.ListHead) { break; }

        HOOK *hook = CONTAINING_RECORD(listEntry, HOOK, ListEntry);

        _HookDisableFromKernel(hook->HookType, &hook->HookId);
        ExFreePoolWithTag(hook, DRIVER_TAG);
    }

    return STATUS_SUCCESS;
}

NTSTATUS
HooksAddOnProcessCreationAndProcessExit(
    PCREATE_PROCESS_NOTIFY_ROUTINE_EX Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // Vezi daca nu cumva avem deja acest hook pus
    HOOK_ID initialHookId; initialHookId.CallbackAddress = (VOID *)Callback;
    if (_ReturnHookListEntry(HOOK_TYPE_PROCESS_CREATE_PROCESS_EXIT, &initialHookId)) { return STATUS_ALREADY_REGISTERED; }

    // Acum ca avem evidenta hook-ului in structurile noastre,
    // roaga kernel-ul sa chiar il faca activ :D
    NTSTATUS status = PsSetCreateProcessNotifyRoutineEx(Callback, FALSE);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("PsSetCreateProcessNotifyRoutineEx failed with status = 0x%x\n", status));
    }
    else
    {
        //
        // Adauga noul hook in lissta
        //
        HOOK_ID hookId = initialHookId; // Acelasi id, adresa callback-ului.
                                        // Nimic mai specific.
        status = _AddNewHookInList(HOOK_TYPE_PROCESS_CREATE_PROCESS_EXIT, &hookId);
    }

    return status;
}

NTSTATUS
HooksRemoveFromProcessCreationAndProcessExit(
    PCREATE_PROCESS_NOTIFY_ROUTINE_EX Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // In caz ca nu avem hook-ul inregistrat in structurile noastre
    // de date, functia de mai jos ar trebui sa faileze. => nu mai are rost sa apelam API-ul de kernel
    HOOK_ID hookId; hookId.CallbackAddress = (VOID *)Callback;
    NTSTATUS status = _RemoveHookFromList(HOOK_TYPE_PROCESS_CREATE_PROCESS_EXIT, &hookId);
    if (!NT_SUCCESS(status)) { return status; }

    // Efectiv dezactiveaza hook-ul
    return PsSetCreateProcessNotifyRoutineEx(Callback, TRUE);
}

NTSTATUS
HooksAddOnThreadCreationAndTermination(
    PCREATE_THREAD_NOTIFY_ROUTINE Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // Vezi daca nu cumva avem deja acest hook pus
    HOOK_ID initialHookId; initialHookId.CallbackAddress = (VOID *)Callback;
    if (_ReturnHookListEntry(HOOK_TYPE_THREAD_CREATE_THREAD_TERMINATE, &initialHookId)) { return STATUS_ALREADY_REGISTERED; }

    // Acum ca avem evidenta hook-ului in structurile noastre,
    // roaga kernel-ul sa chiar il faca activ :D
    NTSTATUS status = PsSetCreateThreadNotifyRoutine(Callback);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("PsSetCreateThreadNotifyRoutine failed with status = 0x%x\n", status));
    }
    else
    {
        //
        // Adauga noul hook in lissta
        //
        HOOK_ID hookId = initialHookId; // Acelasi id, adresa callback-ului.
                                        // Nimic mai specific.
        status = _AddNewHookInList(HOOK_TYPE_THREAD_CREATE_THREAD_TERMINATE, &hookId);
    }

    return status;
}

NTSTATUS
HooksRemoveFromThreadCreationAndTermination(
    PCREATE_THREAD_NOTIFY_ROUTINE Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // In caz ca nu avem hook-ul inregistrat in structurile noastre
    // de date, functia de mai jos ar trebui sa faileze. => nu mai are rost sa apelam API-ul de kernel
    HOOK_ID hookId; hookId.CallbackAddress = (VOID *)Callback;
    NTSTATUS status = _RemoveHookFromList(HOOK_TYPE_THREAD_CREATE_THREAD_TERMINATE, &hookId);
    if (!NT_SUCCESS(status)) { return status; }

    // Efectiv dezactiveaza hook-ul
    return PsRemoveCreateThreadNotifyRoutine(Callback);
}

NTSTATUS
HooksAddOnRegistryKeys(
    PEX_CALLBACK_FUNCTION   Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // Vezi daca nu cumva avem deja acest hook pus
    HOOK_ID initialHookId; initialHookId.CallbackAddress = (VOID *)Callback;
    if (_ReturnHookListEntry(HOOK_TYPE_REGISTRY_KEYS, &initialHookId)) { return STATUS_ALREADY_REGISTERED; }

    // Roaga kernel-ul sa chiar il faca activ, hookul :D
    LARGE_INTEGER specificRegistryHookId;
    static const UNICODE_STRING driverAltitude = RTL_CONSTANT_STRING(MY_FILTER_DUMMY_ALTITUDE);
    NTSTATUS status = CmRegisterCallbackEx(
        Callback,
        &driverAltitude,
        DriverGlobalData->DriverObject,
        NULL,   // Nu vrem sa primim niciun context inapoi
        &specificRegistryHookId,
        NULL
    );
    if (!NT_SUCCESS(status))
    {
        KdPrint(("CmRegisterCallbackEx failed with status = 0x%x\n", status));
    }
    else
    {
        //
        // Adauga noul hook in lista
        //
        HOOK_ID hookId = initialHookId;
        hookId.Registry = specificRegistryHookId;   // Trebuie setat id-ul
                                                    // specific acestui tip de hook.
        status = _AddNewHookInList(HOOK_TYPE_REGISTRY_KEYS, &hookId);
    }

    return status;
}

NTSTATUS
HooksRemoveFromRegistryKeys(
    PEX_CALLBACK_FUNCTION   Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // In caz ca nu avem hook-ul inregistrat in structurile noastre
    // de date, functia de mai jos ar trebui sa faileze.
    HOOK_ID initialHookId; initialHookId.CallbackAddress = (VOID *)Callback;
    LIST_ENTRY *entry = _ReturnHookListEntry(HOOK_TYPE_REGISTRY_KEYS, &initialHookId);
    if (!entry) { return STATUS_ALREADY_DISCONNECTED; }

    // Afla id-ul hook-ului ca sa il putem sterge folosind
    // api-ul kernelului
    LARGE_INTEGER specificRegistryHookId;
    HOOK *hook = CONTAINING_RECORD(entry, HOOK, ListEntry);
    specificRegistryHookId = hook->HookId.Registry;

    // Sterge hook-ul din structurile noastre
    _RemoveHookFromList(HOOK_TYPE_REGISTRY_KEYS, &hook->HookId);

    // Efectiv dezactiveaza hook-ul
    return CmUnRegisterCallback(specificRegistryHookId);
}

NTSTATUS
HooksAddOnImageLoad(
    PLOAD_IMAGE_NOTIFY_ROUTINE Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // Vezi daca nu cumva avem deja acest hook pus
    HOOK_ID initialHookId; initialHookId.CallbackAddress = (VOID *)Callback;
    if (_ReturnHookListEntry(HOOK_TYPE_IMAGE_LOAD, &initialHookId)) { return STATUS_ALREADY_REGISTERED; }

    // Sa rugam kernelul sa ne face hook-ul activ
    NTSTATUS status = PsSetLoadImageNotifyRoutine(Callback);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("PsSetLoadImageNotifyRoutine failed with status = 0x%x\n", status));
    }
    else
    {
        //
        // Adauga noul hook in lista
        //
        HOOK_ID hookId = initialHookId; // Acelasi id, adresa callback-ului.
                                        // Nimic mai specific.
        status = _AddNewHookInList(HOOK_TYPE_IMAGE_LOAD, &hookId);
    }

    return status;
}

NTSTATUS
HooksRemoveFromImageLoad(
    PLOAD_IMAGE_NOTIFY_ROUTINE Callback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // In caz ca nu avem hook-ul inregistrat in structurile noastre
    // de date, functia de mai jos ar trebui sa faileze. => nu mai are rost sa apelam API-ul de kernel
    HOOK_ID hookId; hookId.CallbackAddress = (VOID *)Callback;
    NTSTATUS status = _RemoveHookFromList(HOOK_TYPE_IMAGE_LOAD, &hookId);
    if (!NT_SUCCESS(status)) { return status; }

    // Efectiv dezactiveaza hook-ul
    return PsRemoveLoadImageNotifyRoutine(Callback);
}

NTSTATUS
HooksAddOnProcessHandleOperations(
    POB_PRE_OPERATION_CALLBACK  PreOperationCallback,
    POB_POST_OPERATION_CALLBACK PostOperationCallback
)
{
    if (!HooksGlobalData.IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    // Vezi daca nu cumva avem deja acest hook pus
    HOOK_ID initialHookId; initialHookId.CallbackAddress = (VOID *)PreOperationCallback;
    if (_ReturnHookListEntry(HOOK_TYPE_OB, &initialHookId)) { return STATUS_ALREADY_REGISTERED; }

    // Sa rugam kernelul sa ne face hook-ul activ
    OB_CALLBACK_REGISTRATION obCallbackRegistration;
    obCallbackRegistration.Version = OB_FLT_REGISTRATION_VERSION;   // Drivers should specify OB_FLT_REGISTRATION_VERSION.
    obCallbackRegistration.OperationRegistrationCount = 1;          // Punem numai un set de callback-uri (cele date in parametrii de intrare)
    UNICODE_STRING driverDummyAltitude = RTL_CONSTANT_STRING(MY_FILTER_DUMMY_ALTITUDE);
    obCallbackRegistration.Altitude = driverDummyAltitude;
    obCallbackRegistration.RegistrationContext = NULL;              // Nu folosim, sper ca merge cu NULL si nu se supara

    OB_OPERATION_REGISTRATION obOperationRegistration;
    obOperationRegistration.ObjectType = PsProcessType;             // Vrem sa hookam terminarea de PROCESE
    obOperationRegistration.Operations = OB_OPERATION_HANDLE_CREATE | OB_OPERATION_HANDLE_DUPLICATE;
    obOperationRegistration.PreOperation = PreOperationCallback;
    obOperationRegistration.PostOperation = PostOperationCallback;

    obCallbackRegistration.OperationRegistration = &obOperationRegistration;

    VOID *specificHookId;
    NTSTATUS status = ObRegisterCallbacks(&obCallbackRegistration, &specificHookId);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("ObRegisterCallbacks failed with status = 0x%x\n", status));
    }
    else
    {
        //
        // Adauga noul hook in lista
        //
        HOOK_ID hookId = initialHookId;
        hookId.Ob = specificHookId;
        status = _AddNewHookInList(HOOK_TYPE_OB, &hookId);
    }

    return status;
}

NTSTATUS
HookSpecificMinifilterIrpCode(
    UINT32                       IrpCode,
    PFLT_PRE_OPERATION_CALLBACK  PreCallback,
    PFLT_POST_OPERATION_CALLBACK PostCallback
)
{
    if (!HooksGlobalData.IsComponentInited) return STATUS_UNSUCCESSFUL;

    switch (IrpCode)
    {
    case IRP_MJ_CREATE:
    {
#if(!HOOK_IRP_MJ_CREATE)
        return STATUS_NOT_IMPLEMENTED;
#else
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.AreCallbacksAvailable,
            FALSE);
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.PreCallback = PreCallback;
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.PostCallback = PostCallback;
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.AreCallbacksAvailable,
            TRUE);

        return STATUS_SUCCESS;
#endif
    }

    case IRP_MJ_SET_INFORMATION:
    {
#if(!HOOK_IRP_MJ_SET_INFORMATION)
        return STATUS_NOT_IMPLEMENTED;
#else
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.AreCallbacksAvailable,
            FALSE);
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.PreCallback = PreCallback;
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.PostCallback = PostCallback;
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.AreCallbacksAvailable,
            TRUE);

        return STATUS_SUCCESS;
#endif
    }

    case IRP_MJ_WRITE:
    {
#if(!HOOK_IRP_MJ_WRITE)
        return STATUS_NOT_IMPLEMENTED;
#else
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.AreCallbacksAvailable,
            FALSE);
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.PreCallback = PreCallback;
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.PostCallback = PostCallback;
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.AreCallbacksAvailable,
            TRUE);

        return STATUS_SUCCESS;
#endif
    }

    default:
        return STATUS_NOT_IMPLEMENTED;
    }
}

NTSTATUS
HookRemoveSpecificMinifilterIrpCode(
    UINT32 IrpCode
)
{
    if (!HooksGlobalData.IsComponentInited) return STATUS_UNSUCCESSFUL;

    switch (IrpCode)
    {
    case IRP_MJ_CREATE:
    {
#if(!HOOK_IRP_MJ_CREATE)
        return STATUS_NOT_IMPLEMENTED;
#else
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.AreCallbacksAvailable,
            FALSE);
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.PreCallback = NULL;
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.PostCallback = NULL;

        return STATUS_SUCCESS;
#endif
    }

    case IRP_MJ_SET_INFORMATION:
    {
#if(!HOOK_IRP_MJ_SET_INFORMATION)
        return STATUS_NOT_IMPLEMENTED;
#else
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.AreCallbacksAvailable,
            FALSE);
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.PreCallback = NULL;
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.PostCallback = NULL;

        return STATUS_SUCCESS;
#endif
    }

    case IRP_MJ_WRITE:
    {
#if(!HOOK_IRP_MJ_WRITE)
        return STATUS_NOT_IMPLEMENTED;
#else
        InterlockedExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.AreCallbacksAvailable,
            FALSE);
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.PreCallback = NULL;
        HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.PostCallback = NULL;

        return STATUS_SUCCESS;
#endif
    }

    default:
        return STATUS_NOT_IMPLEMENTED;
    }
}

/* Static functions */
static
LIST_ENTRY*
_ReturnHookListEntry(
    HOOK_TYPE       HookType,
    HOOK_ID         *HookId
)
//
// Ca si hook ID functia se bazeaza doar
// pe adresa callback-ului. Am convenit ca
// exista doar un callback per hook
//
{
    LIST_ENTRY *entry = HooksGlobalData.Hooks.ListHead.Flink;

    while (entry != &HooksGlobalData.Hooks.ListHead)
    {
        HOOK *hook = CONTAINING_RECORD(entry, HOOK, ListEntry);

        if (hook->HookType == HookType && hook->HookId.CallbackAddress == HookId->CallbackAddress)
        {
            return entry;
        }

        entry = entry->Flink;
    }

    return NULL;
}

static
NTSTATUS
_AddNewHookInList(
    HOOK_TYPE       HookType,
    HOOK_ID         *HookId
)
{
    // Allocate space for the new hook
    HOOK *hook = ExAllocatePoolWithTag(PagedPool, sizeof(hook[0]), DRIVER_TAG);
    if (!hook)
    {
        KdPrint(("ExAllocatePoolWithTag failed!"));
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    hook->HookType = HookType;
    if (HookId) { hook->HookId = *HookId; } // ar trebui sa se execute mereu...

    // Insert the new hook in the list
    InsertHeadList(&HooksGlobalData.Hooks.ListHead, &hook->ListEntry);
    HooksGlobalData.Hooks.NumberOfHooks++;

    return STATUS_SUCCESS;
}

static
NTSTATUS
_RemoveHookFromList(
    HOOK_TYPE       HookType,
    HOOK_ID         *HookId
)
{
    LIST_ENTRY *entry = _ReturnHookListEntry(HookType, HookId);

    if (entry)
    {
        HOOK *hook = CONTAINING_RECORD(entry, HOOK, ListEntry);
        RemoveEntryList(entry);
        ExFreePoolWithTag(hook, DRIVER_TAG);

        HooksGlobalData.Hooks.NumberOfHooks--;
        return STATUS_SUCCESS;
    }
    else
    {
        return STATUS_UNSUCCESSFUL;
    }
}

static
VOID
_HookDisableFromKernel(
    HOOK_TYPE       HookType,
    HOOK_ID         *HookId
)
//
// In functie de HookType ar trebui chemata
// functia potrivita ce va face disable la hook
//
{
    switch (HookType)
    {
    case HOOK_TYPE_PROCESS_CREATE_PROCESS_EXIT:
        PsSetCreateProcessNotifyRoutineEx((PCREATE_PROCESS_NOTIFY_ROUTINE_EX)HookId->CallbackAddress, TRUE);
        break;

    case HOOK_TYPE_THREAD_CREATE_THREAD_TERMINATE:
        PsRemoveCreateThreadNotifyRoutine((PCREATE_THREAD_NOTIFY_ROUTINE)HookId->CallbackAddress);
        break;

    case HOOK_TYPE_REGISTRY_KEYS:
        CmUnRegisterCallback(HookId->Registry);
        break;

    case HOOK_TYPE_IMAGE_LOAD:
        PsRemoveLoadImageNotifyRoutine((PLOAD_IMAGE_NOTIFY_ROUTINE)HookId->CallbackAddress);
        break;

    case HOOK_TYPE_OB:
        ObUnRegisterCallbacks(HookId->Ob);
        break;

    default: // nu ar trebui sa ajungem aici
        KdPrint(("\n\n[%s] !!! FORGOT TO UNHOOK HOOK WITH ID = [%d] !!!\n", HookType));
        break;
    }

    return;
}

/****************************************************************/
/*                MiniFilterSpecificCallbacks                   */
/****************************************************************/
#if(HOOK_IRP_MJ_CREATE)
static
FLT_PREOP_CALLBACK_STATUS
_MiniFilterPreOperationIrpMjCreate(
    _Inout_ PFLT_CALLBACK_DATA              Data,
    _In_    PCFLT_RELATED_OBJECTS           FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID    *CompletionContext
)
{
    if (InterlockedCompareExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.AreCallbacksAvailable,
        TRUE, TRUE))
    {
        return HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.PreCallback(
            Data, FltObjects, CompletionContext);
    }
    else
    {
        return FLT_PREOP_SUCCESS_WITH_CALLBACK;
    }
}

static
FLT_POSTOP_CALLBACK_STATUS
_MiniFilterPostOperationIrpMjCreate(
    _Inout_ PFLT_CALLBACK_DATA      Data,
    _In_ PCFLT_RELATED_OBJECTS      FltObjects,
    _In_opt_ PVOID                  CompletionContext,
    _In_ FLT_POST_OPERATION_FLAGS   Flags
)
{
    if (InterlockedCompareExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.AreCallbacksAvailable,
        TRUE, TRUE))
    {
        return HooksGlobalData.MiniFilterSpecificHooks.IrpMjCreate.PostCallback(
            Data, FltObjects, CompletionContext, Flags);
    }
    else
    {
        return FLT_POSTOP_FINISHED_PROCESSING;
    }
}
#endif

#if(HOOK_IRP_MJ_SET_INFORMATION)
static
FLT_PREOP_CALLBACK_STATUS
_MiniFilterPreOperationIrpMjSetInfo(
    _Inout_ PFLT_CALLBACK_DATA              Data,
    _In_    PCFLT_RELATED_OBJECTS           FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
    if (InterlockedCompareExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.AreCallbacksAvailable,
        TRUE, TRUE))
    {
        return HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.PreCallback(
            Data, FltObjects, CompletionContext);
    }
    else
    {
        return FLT_PREOP_SUCCESS_WITH_CALLBACK;
    }
}

static
FLT_POSTOP_CALLBACK_STATUS
_MiniFilterPostOperationIrpMjSetInfo(
    _Inout_ PFLT_CALLBACK_DATA      Data,
    _In_ PCFLT_RELATED_OBJECTS      FltObjects,
    _In_opt_ PVOID                  CompletionContext,
    _In_ FLT_POST_OPERATION_FLAGS   Flags
)
{
    if (InterlockedCompareExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.AreCallbacksAvailable,
        TRUE, TRUE))
    {
        return HooksGlobalData.MiniFilterSpecificHooks.IrpMjSetInformation.PostCallback(
            Data, FltObjects, CompletionContext, Flags);
    }
    else
    {
        return FLT_POSTOP_FINISHED_PROCESSING;
    }
}
#endif

#if(HOOK_IRP_MJ_WRITE)
static
FLT_PREOP_CALLBACK_STATUS
_MiniFilterPreOperationIrpMjWrite(
    _Inout_ PFLT_CALLBACK_DATA              Data,
    _In_    PCFLT_RELATED_OBJECTS           FltObjects,
    _Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
    if (InterlockedCompareExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.AreCallbacksAvailable,
        TRUE, TRUE))
    {
        return HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.PreCallback(
            Data, FltObjects, CompletionContext);
    }
    else
    {
        return FLT_PREOP_SUCCESS_WITH_CALLBACK;
    }
}

static
FLT_POSTOP_CALLBACK_STATUS
_MiniFilterPostOperationIrpMjWrite(
    _Inout_ PFLT_CALLBACK_DATA      Data,
    _In_ PCFLT_RELATED_OBJECTS      FltObjects,
    _In_opt_ PVOID                  CompletionContext,
    _In_ FLT_POST_OPERATION_FLAGS   Flags
)
{
    if (InterlockedCompareExchange(&HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.AreCallbacksAvailable,
        TRUE, TRUE))
    {
        return HooksGlobalData.MiniFilterSpecificHooks.IrpMjWrite.PostCallback(
            Data, FltObjects, CompletionContext, Flags);
    }
    else
    {
        return FLT_POSTOP_FINISHED_PROCESSING;
    }
}
#endif