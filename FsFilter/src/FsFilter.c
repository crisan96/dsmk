#include <fltKernel.h>
#include <dontuse.h>
#include "hooks.h"
#include "global.h"
#include "communication.h"
#include "commands.h"

/*************************************************************************
    MiniFilter initialization and unload routines.
*************************************************************************/

/* Unload routine & specific (with dummy implementation) minifilter functions */
static NTSTATUS _FsFilterUnload(_In_ FLT_FILTER_UNLOAD_FLAGS Flags);

NTSTATUS
DriverEntry(
    _In_ DRIVER_OBJECT  *DriverObject,
    _In_ UNICODE_STRING *RegistryPath
)
{
    UNREFERENCED_PARAMETER(RegistryPath);

    //
    // Init global data
    //
    KdPrint(("Driver is loaded. Init global data...\n"));
    NTSTATUS status = GlobalDataInit(DriverObject);
    if (!NT_SUCCESS(status)) { goto cleanup; }

    //
    // Init hooking system
    //
    KdPrint(("Initing hooks...\n"));
    const FLT_OPERATION_REGISTRATION *miniFilterCallbacks;
    status = HooksInit(&miniFilterCallbacks);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksInit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    //
    // Inregistrarea minifiltrului
    //
#pragma warning(push)
#pragma warning(disable:4204) // nonstandard extension used: non-constant aggregate initializer
    const FLT_REGISTRATION filterRegistration =
    {
        .Size = sizeof(FLT_REGISTRATION),
        .Version = FLT_REGISTRATION_VERSION,

        .Flags = 0,                                     // We do not wand to be aware of Pipes and Mailslots
                                                        // or DAX or DAS

        .ContextRegistration = NULL,
        .OperationRegistration = miniFilterCallbacks,
        .FilterUnloadCallback = _FsFilterUnload,

        .InstanceSetupCallback = NULL,                  // If a NULL is specified for this routine,
                                                        // the attachment is always made.

        .InstanceQueryTeardownCallback = NULL,          // If a NULL is specified for this routine,
                                                        // then instances can never be manually detached. (keep it simple for now)

        .InstanceTeardownStartCallback = NULL,          // Specifying NULL for this callback does not prevent instance teardown

        .InstanceTeardownCompleteCallback = NULL,

        //  The following callbacks are provided by a filter only if it is
        //  interested in modifying the name space.
        //
        //  If NULL is specified for these callbacks, it is assumed that the
        //  filter would not affect the name being requested.
        .GenerateFileNameCallback = NULL,
        .NormalizeNameComponentCallback = NULL,
        .NormalizeContextCleanupCallback = NULL,
#if FLT_MGR_LONGHORN
        .TransactionNotificationCallback = NULL,
        .NormalizeNameComponentExCallback = NULL,
#endif
#if FLT_MGR_WIN8
        .SectionNotificationCallback = NULL,
#endif
    };
#pragma warning(pop)
    KdPrint(("Calling FltRegisterFilter...\n"));
    status = FltRegisterFilter(DriverObject, &filterRegistration, &DriverGlobalData->FilterHandle);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("FltRegisterFilter failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    //
    // Communication init
    //
    KdPrint(("Init communication mechanism...\n"));
    status = CommInit();
    if (!NT_SUCCESS(status))
    {
        KdPrint(("CommInit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    status = CmdInit();
    if (!NT_SUCCESS(status))
    {
        KdPrint(("CmdInit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    KdPrint(("Start filtering from now on...\n"));
    status = FltStartFiltering(DriverGlobalData->FilterHandle);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("FltStartFiltering failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    KdPrint(("Driver Entry done.. All INITs done!\n"));
cleanup:
    if (!NT_SUCCESS(status))
    {
        CmdUninit();
        CommUninit();
        FltUnregisterFilter(DriverGlobalData->FilterHandle);
        HooksUninit();
        GlobalDataUninit();
    }
    return status;
}

static
NTSTATUS
_FsFilterUnload(
    _In_ FLT_FILTER_UNLOAD_FLAGS Flags
)
{
    UNREFERENCED_PARAMETER(Flags);

    KdPrint(("[%s] Uninit in progress...\n", __FUNCTION__));

    CmdUninit();
    CommUninit();
    FltUnregisterFilter(DriverGlobalData->FilterHandle);
    HooksUninit();
    GlobalDataUninit();

    KdPrint(("[%s] Uninit DONE!\n", __FUNCTION__));

    return STATUS_SUCCESS;
}


