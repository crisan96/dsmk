#include "global.h"

DRIVER_GLOBAL_DATA *DriverGlobalData;

/**/
NTSTATUS
GlobalDataInit(
    DRIVER_OBJECT *DriverObject
)
{
    DriverGlobalData = (DRIVER_GLOBAL_DATA *)ExAllocatePoolWithTag(PagedPool, sizeof(DriverGlobalData[0]), DRIVER_TAG);
    if (DriverGlobalData == NULL)
    {
        KdPrint(("ExAllocatePoolWithTag failed!\n"));
        return STATUS_MEMORY_NOT_ALLOCATED;
    }
    memset(DriverGlobalData, 0, sizeof(DriverGlobalData[0]));

    DriverGlobalData->DriverObject = DriverObject;

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
GlobalDataUninit(
    VOID
)
{
    if (DriverGlobalData == NULL) { return STATUS_SUCCESS; }

    ExFreePoolWithTag(DriverGlobalData, DRIVER_TAG);
    DriverGlobalData = NULL;

    return STATUS_SUCCESS;
}