#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <ntddk.h>
#include "common.h"

/* Driver custom tag for memory allocation */
#define DRIVER_TAG '1VRD'

/*
    Device Name
    The device name could be anything but
    should be in the Device object manager directory.
*/
#define DRIVER_DEVICE_NAME L"\\Device\\" DRIVER1_NAME

/* Device symbolic link name */
#define DRIVER_DEVICE_SYM_LINK L"\\??\\" DRIVER1_NAME

typedef struct _DRIVER_GLOBAL_DATA
{
    DEVICE_OBJECT   *DeviceObject;
    UNICODE_STRING  DeviceSymLink;
    FILE_OBJECT     *Driver2FileObject;
    DEVICE_OBJECT   *Driver2DeviceObject;
}DRIVER_GLOBAL_DATA;
extern DRIVER_GLOBAL_DATA *DriverGlobalData;

/**/ NTSTATUS GlobalDataInit(VOID);
/**/ NTSTATUS GlobalDataUninit(VOID);

#endif // !_GLOBAL_H_
