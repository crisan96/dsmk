#ifndef _DISPATCH_ROUTINES_H_
#define _DISPATCH_ROUTINES_H_

#include <ntddk.h>

/**/ NTSTATUS IrpCreate(_In_ DEVICE_OBJECT *DeviceObject, _In_ IRP *Irp);
/**/ NTSTATUS IrpClose(_In_ DEVICE_OBJECT *DeviceObject, _In_ IRP *Irp);
/**/ NTSTATUS IrpDeviceControl(_In_ DEVICE_OBJECT *DeviceObject, _In_ IRP *Irp);

#endif // !_DISPATCH_ROUTINES_H_
