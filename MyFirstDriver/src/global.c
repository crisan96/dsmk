#include "global.h"

DRIVER_GLOBAL_DATA *DriverGlobalData;

/**/
NTSTATUS
GlobalDataInit(
    VOID
)
{
    DriverGlobalData = (DRIVER_GLOBAL_DATA *)ExAllocatePoolWithTag(PagedPool, sizeof(DriverGlobalData[0]), DRIVER_TAG);
    if (!DriverGlobalData)
    {
        KdPrint(("ExAllocatePoolWithTag failed!\n"));
        return STATUS_MEMORY_NOT_ALLOCATED;
    }
    memset(DriverGlobalData, 0, sizeof(DriverGlobalData[0]));

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
GlobalDataUninit(
    VOID
)
{
    if (!DriverGlobalData) { return STATUS_SUCCESS; }

    // pe msdn am vazut ca doar la file object trebuie facut --
    // poate mi se clarifica de ce pe device object nu dupa ca mai
    // citesc din Windows Kernel Programming...
    if (DriverGlobalData->Driver2FileObject) { ObDereferenceObject(DriverGlobalData->Driver2FileObject); }

    // Delete symbolic link
    if (DriverGlobalData->DeviceSymLink.Buffer) { IoDeleteSymbolicLink(&DriverGlobalData->DeviceSymLink); }

    // Delete device object
    if (DriverGlobalData->DeviceObject) { IoDeleteDevice(DriverGlobalData->DeviceObject); }

    ExFreePoolWithTag(DriverGlobalData, DRIVER_TAG);
    DriverGlobalData = NULL;

    return STATUS_SUCCESS;
}