#include <ntddk.h>
#include "global.h"
#include "dispatch_routines.h"
#include "common.h"

/* Unload routine */
VOID MyUnload(_In_ DRIVER_OBJECT *DriverObject);

NTSTATUS
DriverEntry(
    _In_ DRIVER_OBJECT     *DriverObject,
    _In_ UNICODE_STRING    *RegistryPath
)
{
    UNREFERENCED_PARAMETER(RegistryPath);

    //
    // Init global data
    //
    NTSTATUS status = GlobalDataInit();
    if (!NT_SUCCESS(status)) { goto cleanup; }

    //
    // Set unload routine
    //
    DriverObject->DriverUnload = MyUnload;

    //
    // Set dispatch routines
    //
    DriverObject->MajorFunction[IRP_MJ_CREATE] = IrpCreate;
    DriverObject->MajorFunction[IRP_MJ_CLOSE] = IrpClose;
    DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = IrpDeviceControl;

    // A typical software driver needs just one
    // device object, with a symbolic link pointing to it,
    // so that user mode clients can obtain handles.
    UNICODE_STRING deviceName = RTL_CONSTANT_STRING(DRIVER_DEVICE_NAME);
    status = IoCreateDevice(
        DriverObject,                   // our driver object,
        0,                              // no need for extra bytes,
        &deviceName,                    // the device name,
        FILE_DEVICE_UNKNOWN,            // device type,
        0,                              // characteristics flags,
        FALSE,                          // not exclusive,
        &DriverGlobalData->DeviceObject // the resulting pointer
    );
    if (!NT_SUCCESS(status))
    {
        KdPrint(("Failed to create device object (0x%08X)\n", status));
        goto cleanup;
    }

    // If all goes well, we now have a pointer to our device object.
    // The next step is make this device object
    // accessible to user mode callers by providing a symbolic link.
    RtlInitUnicodeString(&DriverGlobalData->DeviceSymLink, DRIVER_DEVICE_SYM_LINK);
    status = IoCreateSymbolicLink(&DriverGlobalData->DeviceSymLink, &deviceName);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("Failed to create symbolic link (0x%08X)\n", status));
        goto cleanup;
    }

    // Ca sa putem trimite IRP-uri spre DRIVER_2
    // avem nevoie de pointerul la device object-ul creat
    // de DRIVER_2.
    // !!! CHEAMA ObDereferenceObject in Unload (mai precis GlobalDataUninit)
    UNICODE_STRING driver2DeviceName = RTL_CONSTANT_STRING(DRIVER2_DEVICE_NAME);
    status = IoGetDeviceObjectPointer(
        &driver2DeviceName,
        FILE_ALL_ACCESS,
        &DriverGlobalData->Driver2FileObject,
        &DriverGlobalData->Driver2DeviceObject
    );
    if (!NT_SUCCESS(status))
    {
        KdPrint(("IoGetDeviceObjectPointer fail! (0x%08X)\n", status));
        goto cleanup;
    }

cleanup:
    if (!NT_SUCCESS(status))
    {
        GlobalDataUninit();
    }
    return status;
}

/* Unload routine */
VOID
MyUnload(
    _In_ DRIVER_OBJECT *DriverObject
)
{
    UNREFERENCED_PARAMETER(DriverObject);

    GlobalDataUninit();

    return;
}
