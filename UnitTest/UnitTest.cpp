#include "pch.h"
#include "CppUnitTest.h"
extern "C"
{
#include "simple_threadpool.h"
#include "ntdll_threadpool.h"
}


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

static DWORD DummyVariable;
static
VOID
_Job1(
    VOID *Argument
)
{
    DummyVariable = *((DWORD *)Argument);
}

typedef struct _JOB_2_ARG
{
    DWORD AiciEste7;
    DWORD AiciEste17;
}JOB_2_ARG;
static JOB_2_ARG DummyVariable2;

static
VOID
CALLBACK
_Job2(
    PTP_CALLBACK_INSTANCE Instance,
    PVOID                 Parameter,
    PTP_WORK              Work
)
{
    UNREFERENCED_PARAMETER(Instance);
    UNREFERENCED_PARAMETER(Work);

    JOB_2_ARG *argument = (JOB_2_ARG *)Parameter;
    DummyVariable2 = *argument;

    return;
}

namespace UnitTest
{
    TEST_CLASS(UnitTest)
    {
    public:

        TEST_METHOD(TestMethod1)
        {
            SimpleThreadpoolInit(4, 32);
            DWORD variable = 7;
            SimpleThreadPoolRegisterJob(_Job1, &variable, sizeof(variable));
            Sleep(1000);
            Assert::IsTrue(DummyVariable == variable);
        }

        TEST_METHOD(TestMethod2)
        {

            NTDLL_THPOOL *threadPoolHandle = NtdllThreadPoolCreate(2, 8);
            JOB_2_ARG *argJob2 = (JOB_2_ARG *)malloc(sizeof(argJob2[0]));
            argJob2->AiciEste7 = 7;
            argJob2->AiciEste17 = 17;
            NtdllThreadPoolRegisterJob(threadPoolHandle, _Job2, argJob2);
            Sleep(1000);
            Assert::IsTrue(DummyVariable2.AiciEste7 == 7 && DummyVariable2.AiciEste7 ==7);
        }
    };
}
