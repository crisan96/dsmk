#ifndef _COMMON_H_
#define _COMMON_H_

#ifdef USER_MODE
#include <Windows.h>
#else
#include <ntddk.h>
#endif

#define MYFILTER_NAME               L"MyFilter"
#define DRIVER1_NAME                L"MyFirstDriver"
#define DRIVER2_NAME                L"MySecondDriver"

#define MYFYLTER_PATH               L"\\\\.\\" MYFILTER_NAME
#define DRIVER_PATH                 L"\\\\.\\" DRIVER1_NAME

/* File System MiniFilter */
#define FS_MINIFILTER_NAME          L"FsFilter"
#define FS_MINIFILTER_DEVICE_NAME   L"\\Device\\" FS_MINIFILTER_NAME
#define FS_MINIFILTER_COMM_PORT     L"\\" FS_MINIFILTER_NAME L"CommPort"

/* Driver1 need this in order to "connect" to the Driver2 */
#define DRIVER2_DEVICE_NAME         L"\\Device\\" DRIVER2_NAME

#define MY_FILTER_DUMMY_ALTITUDE    L"300000"

/* IOCTL codes */
#define DRIVER_FUNCTION_1           CTL_CODE(0x8000, 0x800, METHOD_NEITHER, FILE_ANY_ACCESS)
#define DRIVER_FUNCTION_2           CTL_CODE(0x8000, 0x801, METHOD_NEITHER, FILE_ANY_ACCESS)
#define MY_FILTER_SEND_COMMAND      CTL_CODE(0x8000, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)

//
// Comenzile disponibile.
// Acest fisier este folosit de mai multe
// drivere din solutie, deci unele comenzi sunt
// disponibile doar la unele drivere (ar trebui ca
// driverele sa raspunda doar la ce stiu ele)
//
typedef enum _COMMAND_ID
{
    COMMAND_PROTECT_PROCESS_FROM_TERMINATION,

    COMMAND_PING,

    COMMAND_START_PROCESS_MONITOR,
    COMMAND_STOP_PROCESS_MONITOR,

    COMMAND_START_THREADS_MONITOR,
    COMMAND_STOP_THREADS_MONITOR,

    COMMAND_START_IMAGE_LOAD_MONITOR,
    COMMAND_STOP_IMAGE_LOAD_MONITOR,

    COMMAND_START_REGISTRY_MONITOR,
    COMMAND_STOP_REGISTRY_MONITOR,

    COMMAND_START_FILES_MONITOR,
    COMMAND_STOP_FILES_MONITOR,

    COMMAND_FILE_DELETE_MONITOR,

    COMMAND_SET_PROTECTION_DIRECTORY,
}COMMAND_ID;

//
// Structura ce se trimite driver-ului
// Ea este compusa dintr-un camp numit CommandId
// si mai multe structuri, fiecare structura fiind specifica
// unui tip de comanda.
//
#pragma warning(push)
#pragma warning(disable:4201)   // disable nameless struct warning
#define MAX_SHARED_BUFFER_LENGTH 512
typedef struct _DATA_PACKET
{
    COMMAND_ID CommandId;

    union
    {
        struct
        {
            UINT64  ProcessIdToBeProtectedFromKill;
        }ProtectProcessCmd;

        struct
        {
            // Nu avem structura de input

            struct
            {
                char HelloMsg[MAX_SHARED_BUFFER_LENGTH];
            }Output;
        }PingCmd;

        // Pentru mesajele trimise
        // catre thread-ul consumator (=> informatii
        // despre procese, threaduri, registrii etc.)
        struct
        {
            WCHAR Message[MAX_SHARED_BUFFER_LENGTH];
        }ConsumerThread;

        struct
        {
            struct
            {
                BOOLEAN EnableProtection;   ///< TRUE => protejeaz DirDosName.
                                            ///< FALSE => scoate de la protectie DirDosName (daca era pus)

                WCHAR   DirDosName[MAX_SHARED_BUFFER_LENGTH];
            }Input;

            struct
            {
                NTSTATUS Result;
                WCHAR   ErrorMsg[MAX_SHARED_BUFFER_LENGTH]; ///< daca !NT_SUCCESS(Result)
            }Output;
        }SetProtectionDirectory;
    }Parameters;

    UINT32  Reserved;
}DATA_PACKET;
static_assert(sizeof(DATA_PACKET) < 4096, "DATA_PACKET bigger than PAGE SIZE!\n");
#pragma warning(pop)

#endif // !_COMMON_H_
