#include "ntdll_threadpool.h"
#include <ntstatus.h>
#include <winnt.h>
#include <winternl.h>
#include <stdio.h>

typedef struct _NTDLL_THREAD_POOL
{
    TP_POOL             *ThreadPoolPointer;
    TP_CALLBACK_ENVIRON CallBackEnviron;
}NTDLL_THPOOL;

/**/
NTDLL_THPOOL*
NtdllThreadPoolCreate(
    DWORD MinimumNumberOfThreads,
    DWORD MaximumNumberOfThreads
)
{
    BOOLEAN functionSucceeded = TRUE;

    NTDLL_THPOOL *threadPoolHandle = (NTDLL_THPOOL *)malloc(sizeof(threadPoolHandle[0]));
    if (!threadPoolHandle)
    {
        functionSucceeded = FALSE;
        goto cleanup;
    }

    threadPoolHandle->ThreadPoolPointer = CreateThreadpool(NULL);
    if (!threadPoolHandle->ThreadPoolPointer)
    {
        functionSucceeded = FALSE;
        goto cleanup;
    }

    SetThreadpoolThreadMaximum(threadPoolHandle->ThreadPoolPointer, MaximumNumberOfThreads);
    if (!SetThreadpoolThreadMinimum(threadPoolHandle->ThreadPoolPointer, MinimumNumberOfThreads))
    {
        functionSucceeded = FALSE;
        goto cleanup;
    }

    //
    // Associate the callback environment with our thread pool.
    //
    InitializeThreadpoolEnvironment(&threadPoolHandle->CallBackEnviron);
    SetThreadpoolCallbackPool(&threadPoolHandle->CallBackEnviron, threadPoolHandle->ThreadPoolPointer);

cleanup:
    if (!functionSucceeded)
    {
        if (threadPoolHandle)
        {
            free(threadPoolHandle);
            threadPoolHandle = NULL;
        }
    }

    return threadPoolHandle;
}

/**/
NTSTATUS
NtdllThreadPoolRegisterJob(
    NTDLL_THPOOL        *ThreadPoolHandle,
    PTP_WORK_CALLBACK   Job,
    VOID                *Argument
)
{
    if (!ThreadPoolHandle) { return STATUS_INVALID_PARAMETER_1; }
    if (!Job) { return STATUS_INVALID_PARAMETER_2; }

    //
    // Create work with the callback environment.
    //
    TP_WORK *work = CreateThreadpoolWork(
        Job,
        Argument,
        &ThreadPoolHandle->CallBackEnviron
    );
    if (!work) { return STATUS_UNSUCCESSFUL; }

    //
    // Submit the work to the pool. Because this was a pre-allocated
    // work item (using CreateThreadpoolWork), it is guaranteed to execute.
    //
    SubmitThreadpoolWork(work);

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
NtdllThreadPoolDestroy(
    NTDLL_THPOOL **ThreadPoolHandle
)
{
    if (!ThreadPoolHandle) { return STATUS_INVALID_PARAMETER_1; }

    CloseThreadpool((*ThreadPoolHandle)->ThreadPoolPointer);
    free(*ThreadPoolHandle);
    *ThreadPoolHandle = NULL;

    return STATUS_SUCCESS;
}