#include "simple_threadpool.h"
#include <ntstatus.h>
#include <winnt.h>
#include <winternl.h>
#include <stdio.h>

#define SECONDS_TO_MILLISECONDS(Seconds)    ((Seconds) * 1000)

typedef struct _WORKER
{
#define SIGNAL_CAN_TERMINATE    1
#define SIGNAL_CAN_CONTINUE     0
    __declspec(align(32)) volatile LONG         WorkerShouldTerminate;
    HANDLE                                      WorkerThreadHandle;
    DWORD                                       WorkerId;
}WORKER;

typedef struct _JOB
{
    HANDLE      Lock;
    BOOLEAN     IsValid;
    VOID        *Argument;
    PFUNC_Job   FunctionToExecute;
}JOB;

typedef struct _SIMPLE_THREADPOOL_GLOBAL_DATA
{
    struct
    {
        DWORD   MaxJobs;
        HANDLE  Semaphore;          // Semaforul ne zice cate joburi sunt in "coada".
                                    // Daca semaforul permite 3 thread-uri sa intre in "coada" ar
                                    // insemna ca sunt 3 job-uri de procesat.

        JOB     *ThreadpoolJobs;    // Simularea "cozii" de job-uri. Ar fi de preferat o coada ce sa suporte
                                    // accese concurente/paralele. Pentru simplitate am pus un lacat
                                    // in structura de JOB.
    }Jobs;

    struct
    {
        DWORD   NumberOfWorkerThreads;
        WORKER  *WorkersThreads;
    }Workers;
}SIMPLE_THREADPOOL_GLOBAL_DATA;

/* If this is NULL => component is not inited */
static SIMPLE_THREADPOOL_GLOBAL_DATA *SimpleThreadPoolGlobalData = NULL;

/* IDDLE function. This is where workers threads stay if no JOB to run */
DWORD WINAPI _IddleFunction( LPVOID Param );

/* Static functions */
static                  VOID    _CleanupAllTheData(VOID);
static                  JOB*    _FindFirstAvailableJob(VOID);
static __forceinline    BOOLEAN _IsComponentInited(VOID);
static __forceinline    BOOLEAN _CurrentWorkerShouldTerminate(WORKER *WorkerInfo);
static __forceinline    VOID    _SignalWorkerToTerminate(WORKER *WorkerInfo);

/**/
NTSTATUS
SimpleThreadpoolInit(
    BYTE NumberOfWorkerThreads,
    DWORD MaxNumberOfJobs
)
{
    // Pentru simplitate suportam doar o instanta de threadpool.
    // Pe viitor functia ar putea returna un handle catre o instanta de threadpool.
    if (_IsComponentInited()) { return STATUS_ALREADY_INITIALIZED; }

    NTSTATUS status = STATUS_SUCCESS;

    // Alloc space for the global data structure
    SimpleThreadPoolGlobalData = (SIMPLE_THREADPOOL_GLOBAL_DATA *)malloc(sizeof(SimpleThreadPoolGlobalData[0]));
    if (!SimpleThreadPoolGlobalData)
    {
        status = STATUS_MEMORY_NOT_ALLOCATED;
        goto cleanup;
    }

    // Semaforul pentru joburi. Valoarea semaforului reprezinta
    // numarul de joburi din "coada"
    SimpleThreadPoolGlobalData->Jobs.Semaphore = CreateSemaphore(
        NULL,               // the handle cannot be inherited by child processes
        0,                  // currently no job in the "queue"
        MaxNumberOfJobs,    // maximum count for semaphore <=> maximum jobs
        NULL                // the semaphore object is created without a name.
    );
    if (!SimpleThreadPoolGlobalData->Jobs.Semaphore)
    {
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

    // Spatiu pentru "coada" de job-uri.
    SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs =
        (JOB *)malloc( sizeof(SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[0]) * MaxNumberOfJobs );
    if (!SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs)
    {
        status = STATUS_MEMORY_NOT_ALLOCATED;
        goto cleanup;
    }
    // Iteram "coada" de joburi pentru a semnala ca
    // joburile nu sunt valide si, mai importat, pentru a initializa
    // lacatul job-urilor.
    for (DWORD i = 0; i < MaxNumberOfJobs; ++i)
    {
        SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[i].IsValid = FALSE;
        SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[i].Lock = CreateMutex(
            NULL,               // the handle cannot be inherited by child processes
            FALSE,              // no initial owner of the mutex
            NULL                // the mutex object is created without a name.
        );
        if (!SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[i].Lock)
        {
            status = STATUS_UNSUCCESSFUL;
            goto cleanup;
        }
    }
    SimpleThreadPoolGlobalData->Jobs.MaxJobs = MaxNumberOfJobs;

    // Alocam spatiu unde retinem unele informatii pentru fiecare
    // thread worker in parte
    SimpleThreadPoolGlobalData->Workers.WorkersThreads =
        (WORKER *)malloc( sizeof(SimpleThreadPoolGlobalData->Workers.WorkersThreads[0]) * NumberOfWorkerThreads );
    if (!SimpleThreadPoolGlobalData->Workers.WorkersThreads)
    {
        status = STATUS_MEMORY_NOT_ALLOCATED;
        goto cleanup;
    }
    for (BYTE i = 0; i < NumberOfWorkerThreads; ++i)
    {
        SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerShouldTerminate = FALSE;
        SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerThreadHandle = CreateThread(
            NULL,                                                           // the handle cannot be inherited by child processes
            0,                                                              // the new thread uses the default size for the executable.
            _IddleFunction,                                                 // where every worker starts its work
            &SimpleThreadPoolGlobalData->Workers.WorkersThreads[i],         // current thread arguments
            0,                                                              // the thread runs immediately after creation.
            &SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerId // save the worker id
        );
        if (!SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerThreadHandle)
        {
            status = STATUS_UNSUCCESSFUL;
            goto cleanup;
        }
    }
    SimpleThreadPoolGlobalData->Workers.NumberOfWorkerThreads = NumberOfWorkerThreads;

cleanup:
    if (!NT_SUCCESS(status))
    {
        /* Cleanup all the data */
        _CleanupAllTheData();
    }

    return status;
}

/**/
NTSTATUS
SimpleThreadPoolRegisterJob(
    PFUNC_Job   Job,
    VOID        *Argument,
    DWORD       SizeOfArgument
)
{
    NTSTATUS status = STATUS_SUCCESS;

    if (!_IsComponentInited()) { return STATUS_UNSUCCESSFUL; }

    // Find a free entry in job "queue"
    // Parcurgem doar o data sirul de joburi si
    // APELAM FUNCTIA DE WAIT CU TIMP 0. Deci daca avem
    // ghinionul sa prindem printre primele intrari locuri care atunci se elibereaza,
    // acestea au lacatul luat si trecem peste, => posibil sa raspundem ca nu mai avem loc
    // in "coada" dar defapt sa mai fie... facem asa pt simplitate
    JOB *jobEntry = NULL;
    for (DWORD entryIndex = 0; entryIndex < SimpleThreadPoolGlobalData->Jobs.MaxJobs; ++entryIndex)
    {
        DWORD waitStatus = WaitForSingleObject(
            SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].Lock,
            0   // Nu asteptam. Daca job-ul e in procesare incercam alta intrare. Asta poate fi problematic.
                // Vezi o explicatie mai sus..
        );
        switch (waitStatus)
        {
        case WAIT_TIMEOUT:
        {
            continue;
        }
        case WAIT_OBJECT_0:
        {
            break;
        }
        default:
        {
            // Ceva eroare...
            status = STATUS_UNSUCCESSFUL;
            goto cleanup;
        }
        }

        // Vezi daca intrarea este folosita sau nu
        if (SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].IsValid)
        {
            ReleaseMutex(SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].Lock);
            continue;
        }
        else
        {
            // Daca intrarea nu este folosita, am gasit ce cautam: o intrare libera unde
            // putem plasa noul job
            jobEntry = &SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex];
            break;
        }
    }

    if (!jobEntry)
    {
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

    // Register the new job
    if (Argument)
    {
        jobEntry->Argument = malloc(SizeOfArgument);
        if (!jobEntry->Argument)
        {
            ReleaseMutex(jobEntry->Lock);
            status = STATUS_UNSUCCESSFUL;
            goto cleanup;
        }
        memcpy(jobEntry->Argument, Argument, SizeOfArgument);
    }
    else
    {
        jobEntry->Argument = NULL;
    }

    jobEntry->FunctionToExecute = Job;
    jobEntry->IsValid = TRUE;
    ReleaseMutex(jobEntry->Lock);

    // Signal the semaphore that we have one new job
    ReleaseSemaphore(SimpleThreadPoolGlobalData->Jobs.Semaphore, 1, NULL);

cleanup:
    return status;
}

/**/
NTSTATUS
SimpleThreadUninit(
    VOID
)
{
    if (!_IsComponentInited()) { return STATUS_UNSUCCESSFUL; }

    for (BYTE i = 0; i < SimpleThreadPoolGlobalData->Workers.NumberOfWorkerThreads; ++i)
    {
        _SignalWorkerToTerminate(&SimpleThreadPoolGlobalData->Workers.WorkersThreads[i]);

        DWORD waitStatus = WaitForSingleObject(
            SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerThreadHandle,
            INFINITE
        );
        if (waitStatus != WAIT_OBJECT_0)
        {
            return STATUS_UNSUCCESSFUL;
        }
    }

    /* Cleanup all the data */
    _CleanupAllTheData();

    return STATUS_SUCCESS;
}

/* IDDLE function. This is where workers threads stay if no JOB to run */
DWORD
WINAPI
_IddleFunction(
    LPVOID Param
)
{
#define TIME_TO_WAIT_BEFORE_CHECK_FOR_TERMINATE SECONDS_TO_MILLISECONDS(1)
    WORKER *currentWorkerInfo = (WORKER *)Param;

    while (TRUE)
    {
        while (TRUE)
        {
            DWORD waitStatus = WaitForSingleObject(
                SimpleThreadPoolGlobalData->Jobs.Semaphore,
                TIME_TO_WAIT_BEFORE_CHECK_FOR_TERMINATE
            );
            if (_CurrentWorkerShouldTerminate(currentWorkerInfo)) { goto _exit_thread; }

            if (waitStatus == WAIT_TIMEOUT)
            {
                continue;
            }
            else if (waitStatus == WAIT_OBJECT_0)
            {
                break;
            }
            else
            {
                goto _exit_thread;
            }
        }
        //
        // We have work to do
        //

        // Gaseste primul job valid care trebuie executat
        // Primim inapoi pointer la job si suntem ASIGURATI CA LACATUL JOB-ULUI E DEJA LUAT
        JOB *jobToExecute = _FindFirstAvailableJob();
        if (!jobToExecute)
        {
            // Nu prea ar trebui sa ajungem aici...
            continue;
        }

        // Executa job-ul cu argumentele date..
        jobToExecute->FunctionToExecute(jobToExecute->Argument);

        // Nu uita sa faci release la lacatul job-ului
        // + sa faci release la memoria argumentului
        // + sa semnalezi ca job-ul e gata => nu mai e valid
        if (jobToExecute->Argument) { free(jobToExecute->Argument); }
        jobToExecute->IsValid = FALSE;
        ReleaseMutex(jobToExecute->Lock);
    }

_exit_thread:
    return 0;
}

/* Static functions */
static
__forceinline
BOOLEAN
_IsComponentInited(
    VOID
)
{
    return SimpleThreadPoolGlobalData != NULL;
}

static
__forceinline
BOOLEAN
_CurrentWorkerShouldTerminate(
    WORKER *WorkerInfo
)
{
    return
        InterlockedCompareExchange(&WorkerInfo->WorkerShouldTerminate, SIGNAL_CAN_CONTINUE, SIGNAL_CAN_TERMINATE)
        == SIGNAL_CAN_TERMINATE ?
        TRUE : FALSE;
}

static
JOB*
_FindFirstAvailableJob(
    VOID
)
{
    for (DWORD entryIndex = 0; entryIndex < SimpleThreadPoolGlobalData->Jobs.MaxJobs; ++entryIndex)
    {
        DWORD waitStatus = WaitForSingleObject(
            SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].Lock,
            0   // Nu asteptam. Daca job-ul e in procesare incercam alta intrare. Asta poate fi problematic.
                // Vezi o explicatie mai sus..
        );
        switch (waitStatus)
        {
        case WAIT_TIMEOUT:
        {
            continue;
        }
        case WAIT_OBJECT_0:
        {
            break;
        }
        default:
        {
            // Ceva eroare...
            return NULL;
        }
        }

        if (SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].IsValid)
        {
            return &SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex];
        }
    }

    return NULL;
}

static
__forceinline
VOID
_SignalWorkerToTerminate(
    WORKER *WorkerInfo
)
{
    InterlockedExchange(&WorkerInfo->WorkerShouldTerminate, SIGNAL_CAN_TERMINATE);
}

static
VOID
_CleanupAllTheData(
    VOID
)
{
    if (SimpleThreadPoolGlobalData)
    {
        // Workers
        if (SimpleThreadPoolGlobalData->Workers.WorkersThreads)
        {
            for(BYTE i = 0; i < SimpleThreadPoolGlobalData->Workers.NumberOfWorkerThreads; ++i)
            {
                CloseHandle(SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerThreadHandle);
            }
            free(SimpleThreadPoolGlobalData->Workers.WorkersThreads);
        }

        // Jobs
        if (SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs)
        {
            for (DWORD i = 0; i < SimpleThreadPoolGlobalData->Jobs.MaxJobs; ++i)
            {
                CloseHandle(SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[i].Lock);
            }
            CloseHandle(SimpleThreadPoolGlobalData->Jobs.Semaphore);
            free(SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs);
        }

        free(SimpleThreadPoolGlobalData);
    }

    // This will tell to the users that the component is now uninited
    SimpleThreadPoolGlobalData = NULL;
}