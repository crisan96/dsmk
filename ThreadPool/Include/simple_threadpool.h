#ifndef _SIMPLE_THREADPOOL_H_
#define _SIMPLE_THREADPOOL_H_

#define WIN32_NO_STATUS
#include <Windows.h>
#undef WIN32_NO_STATUS

//
// How the prototype of the function you can register looks like
// to be able to run by threadpool
// So, you define your function who take an VOID* argument
//
typedef VOID (*PFUNC_Job)(VOID *Argument);

/* Functions */

// Prima functie ce trebuie apelata. Initializeaza threadpool-ul
// - NumberOfWorkerThreads: Cate threaduri vor fi create
//                          pentru a lucra in cadrul threadpool-ului
//
// - MaxNumberOfJobs:       Numarul maxim de joburi ce pot fi introduse
//                          spre a fi executate
/**/ NTSTATUS SimpleThreadpoolInit(BYTE NumberOfWorkerThreads, DWORD MaxNumberOfJobs);

// Functie cu ajutorul careia putem adauga job-uri spre a fi executate.
// - Job:                   Efectiv functia care vrem sa fie executata.
//                          Ea trebuie sa fie de tipul PFUNC_Job.
//
// - Argument:              Argumentul ce ii va fi pasat functiei Job.
//                          Apelantul nu trebuie sa-si faca griji de pointerul ce l-a trimis
//                          aici sa ramana valid pe parcursul executiei programului. Entitatea de
//                          threadpool se va ocupa de asta.
//                          Se poate trimite NULL daca nu se foloseste de catre Job.
//
// - SizeOfArgument:        Dimensiunea in BYTES a valorii gasite la adresa din Argument.
/**/ NTSTATUS SimpleThreadPoolRegisterJob(PFUNC_Job Job, VOID *Argument, DWORD SizeOfArgument);

// Functia prin care dezinitializam threadpool-ul
/**/ NTSTATUS SimpleThreadUninit(VOID);

#endif // !_SIMPLE_THREADPOOL_H_
