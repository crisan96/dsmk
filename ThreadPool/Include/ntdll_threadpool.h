#ifndef _NTDLL_THREADPOOL_H_
#define _NTDLL_THREADPOOL_H_

#define WIN32_NO_STATUS
#include <Windows.h>
#undef WIN32_NO_STATUS

// From MDSN: To compile an application that uses this function, define _WIN32_WINNT as 0x0600 or higher.
#undef  _WIN32_WINNT
#define _WIN32_WINNT 0x0600

//
// Structura ce va fi returnata (mai precis pointer) de catre
// functia de creare. Ea va fi folosita ca si handle catre
// threadpool-ul creat. Folosind acest handle putem inregistra
// job-uri la o anumita instanta de threadpool
//
typedef struct _NTDLL_THREAD_POOL NTDLL_THPOOL;

// Functie ce creaza un threadpool si returneaza un
// "handle" spre acesta
// - MinimumNumberOfThreads:        Cate threaduri ar trebui sa aiba minim threadpool-ul.
// - MaximumNumberOfThreads:        Cate threaduri ar trebui sa aiba maxim threadpool-ul.
// Returneaza:                      Handle catre threadpool sau NULL in caz de esec.
/**/ NTDLL_THPOOL*  NtdllThreadPoolCreate(DWORD MinimumNumberOfThreads, DWORD MaximumNumberOfThreads);

// Functie prin care putem inregistra un job pentru a fi executat in cadrul
// unui threadpool.
// - ThreadPoolHandle:              "Handle" catre un threadpool creat cu functia NtdllThreadPoolCreate.
// - Job:                           Efectiv functia ce va fi executata
// - Argument:                      Argumentul pasat functiei Job
/**/ NTSTATUS       NtdllThreadPoolRegisterJob(NTDLL_THPOOL *ThreadPoolHandle, PTP_WORK_CALLBACK Job, VOID *Argument);

// Functie pentru distrugerea unui threadpool
// - ThreadPoolHandle:              Pointer la handle returnat de functia NtdllThreadPoolCreate
/**/ NTSTATUS       NtdllThreadPoolDestroy(NTDLL_THPOOL **ThreadPoolHandle);

#endif // !_NTDLL_THREADPOOL_H_
