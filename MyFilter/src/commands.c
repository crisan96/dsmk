#include "commands.h"
#include "hooks.h"

//Process Security and Access Rights
#define PROCESS_CREATE_THREAD  (0x0002)
#define PROCESS_CREATE_PROCESS (0x0080)
#define PROCESS_TERMINATE      (0x0001)
#define PROCESS_VM_WRITE       (0x0020)
#define PROCESS_VM_READ        (0x0010)
#define PROCESS_VM_OPERATION   (0x0008)
#define PROCESS_SUSPEND_RESUME (0x0800)

static BOOLEAN  IsComponentInited;
static UINT64   ProtectedProcessID;

/* Callbacks */
OB_PREOP_CALLBACK_STATUS    _PreProcessHandleOperationCallback(_In_ PVOID RegistrationContext, _Inout_ POB_PRE_OPERATION_INFORMATION OperationInformation);
VOID                        _PostProcessHandleOperationCallback(_In_ PVOID RegistrationContext, _In_ POB_POST_OPERATION_INFORMATION OperationInformation);

/**/
NTSTATUS
CommandsInit(
    VOID
)
{
    //
    // Pentru a putea satisface unele comenzi, avem nevoie de callback-uri
    // ce sunt apelate de catre mecanismul de hook-are. De exemplu, daca vrem
    // sa protejam un proces ca sa nu poata fi omorat, o sa punem un hook pe
    // operatia ce deschide handle-urile de procese si acelui handle ii
    // vom spune ca nu are drepturi de terminare
    //
    NTSTATUS status = HooksAddOnProcessHandleOperations(_PreProcessHandleOperationCallback, _PostProcessHandleOperationCallback);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnProcessCreationAndProcessExit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

cleanup:
    if (NT_SUCCESS(status))
    {
        IsComponentInited = TRUE;
    }

    return status;
}

/**/
NTSTATUS
CommandsUninit(
    VOID
)
{
    // Deocamdata nimic aici..
    return STATUS_SUCCESS;
}

/**/
NTSTATUS
CommandsProtectProcessFromTermination(
    UINT64 ProcessID
)
{
    if (!IsComponentInited) { return STATUS_UNSUCCESSFUL; }

    if (ProcessID == 0) { return STATUS_INVALID_PARAMETER; } // nu stiu exact ce pid-uri is invalide..

    ProtectedProcessID = ProcessID;

    return STATUS_SUCCESS;
}

/**/ NTSTATUS
CommandsDeviceControlIrpHandler(
    DATA_PACKET *DataPacket
)
{
    if (!DataPacket) { return STATUS_INVALID_PARAMETER_1; }

    switch (DataPacket->CommandId)
    {
    case COMMAND_PROTECT_PROCESS_FROM_TERMINATION:
        ProtectedProcessID = DataPacket->Parameters.ProtectProcessCmd.ProcessIdToBeProtectedFromKill;
        KdPrint(("[COMMANDS] We have to protect process with ID = [%lld]\n", ProtectedProcessID));
        break;

    default:
        break;
    }

    return STATUS_SUCCESS;
}

/* Callbacks */
OB_PREOP_CALLBACK_STATUS
_PreProcessHandleOperationCallback(
    _In_    PVOID                           RegistrationContext,
    _Inout_ POB_PRE_OPERATION_INFORMATION   OperationInformation
)
{
    UNREFERENCED_PARAMETER(RegistrationContext);

    HANDLE currentProcessId = PsGetProcessId((PEPROCESS)OperationInformation->Object);

    ACCESS_MASK *desiredAccessMask = (OperationInformation->Operation == OB_OPERATION_HANDLE_CREATE) ?
        &OperationInformation->Parameters->CreateHandleInformation.DesiredAccess :
        &OperationInformation->Parameters->DuplicateHandleInformation.DesiredAccess;

    if (!OperationInformation->KernelHandle && ProtectedProcessID == (UINT64)currentProcessId)
    {
        KdPrint(("[COMMANDS] BLOCK the process (PID = [%lld]) termination!\n", ProtectedProcessID));

        *desiredAccessMask &= ~PROCESS_TERMINATE;

        ProtectedProcessID = 0;
    }

    return OB_PREOP_SUCCESS;
}

VOID
_PostProcessHandleOperationCallback(
    _In_ PVOID                          RegistrationContext,
    _In_ POB_POST_OPERATION_INFORMATION OperationInformation
)
{
    UNREFERENCED_PARAMETER(RegistrationContext);
    UNREFERENCED_PARAMETER(OperationInformation);

    return;
}