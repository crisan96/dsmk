#include <ntddk.h>
#include "global.h"
#include "hooks.h"
#include "callbacks.h"
#include "dispatch_routines.h"
#include "commands.h"

/* Unload routine */
VOID MyUnload(_In_ DRIVER_OBJECT *DriverObject);

NTSTATUS
DriverEntry(
    _In_ DRIVER_OBJECT     *DriverObject,
    _In_ UNICODE_STRING    *RegistryPath
)
{
    UNREFERENCED_PARAMETER(RegistryPath);

    //
    // Init global data
    //
    KdPrint(("Driver is loaded. Init global data...\n"));
    NTSTATUS status = GlobalDataInit(DriverObject);
    if (!NT_SUCCESS(status)) { goto cleanup; }

    //
    // Set unload routine
    //
    DriverObject->DriverUnload = MyUnload;

    //
    // Set dispatch routines
    //
    DriverObject->MajorFunction[IRP_MJ_CREATE] = IrpCreate;
    DriverObject->MajorFunction[IRP_MJ_CLOSE] = IrpClose;
    DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = IrpDeviceControl;

    // A typical software driver needs just one
    // device object, with a symbolic link pointing to it,
    // so that user mode clients can obtain handles.
    UNICODE_STRING deviceName = RTL_CONSTANT_STRING(DRIVER_DEVICE_NAME);
    status = IoCreateDevice(
        DriverObject,                   // our driver object,
        0,                              // no need for extra bytes,
        &deviceName,                    // the device name,
        FILE_DEVICE_UNKNOWN,            // device type,
        0,                              // characteristics flags,
        FALSE,                          // not exclusive,
        &DriverGlobalData->DeviceObject // the resulting pointer
    );
    if (!NT_SUCCESS(status))
    {
        KdPrint(("Failed to create device object (0x%08X)\n", status));
        goto cleanup;
    }

    // If all goes well, we now have a pointer to our device object.
    // The next step is make this device object
    // accessible to user mode callers by providing a symbolic link.
    RtlInitUnicodeString(&DriverGlobalData->DeviceSymLink, DRIVER_DEVICE_SYM_LINK);
    status = IoCreateSymbolicLink(&DriverGlobalData->DeviceSymLink, &deviceName);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("Failed to create symbolic link (0x%08X)\n", status));
        goto cleanup;
    }

    //
    // Set hooks (on process creation, registry key etc...)
    //
    KdPrint(("Initing hooks...\n"));
    status = HooksInit();
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksInit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    KdPrint(("Register process creation/exit hook...\n"));
    status = HooksAddOnProcessCreationAndProcessExit(CallbackProcessCreationAndProcessExit);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnProcessCreationAndProcessExit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    KdPrint(("Register registry keys hook...\n"));
    status = HooksAddOnRegistryKeys(CallbackRegistryKeys);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnProcessCreationAndProcessExit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    KdPrint(("Register load image in memory hook...\n"));
    status = HooksAddOnImageLoad(CallbackLoadImage);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("HooksAddOnProcessCreationAndProcessExit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

    //
    // Initialiam entitatea ce primeste comenzi
    //
    KdPrint(("Initing commands entity...\n"));
    status = CommandsInit();
    if (!NT_SUCCESS(status))
    {
        KdPrint(("CommandsInit failed with status = (0x%X)\n", status));
        goto cleanup;
    }

cleanup:
    if (!NT_SUCCESS(status))
    {
        CommandsUninit();
        HooksUninit();
        GlobalDataUninit();
    }
    return status;
}

/* Unload routine */
VOID
MyUnload(
    _In_ DRIVER_OBJECT *DriverObject
)
{
    UNREFERENCED_PARAMETER(DriverObject);

    KdPrint(("Unload driver...\n"));

    CommandsUninit();

    HooksUninit();

    GlobalDataUninit();

    return;
}
