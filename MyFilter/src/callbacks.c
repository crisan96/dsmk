#include "callbacks.h"

/* Static functions */
static NTSTATUS _HandleRegRenameKey(REG_RENAME_KEY_INFORMATION *RegRenameKeyInfo);

VOID
CallbackProcessCreationAndProcessExit(
    _Inout_     PEPROCESS               Process,
    _In_        HANDLE                  ProcessId,
    _Inout_opt_ PS_CREATE_NOTIFY_INFO   *CreateInfo
)
{
    UNREFERENCED_PARAMETER(Process);

    if (CreateInfo)
    {
        KdPrint(("\n\nProcess create with ID = [%p]!\n", ProcessId));

        if (CreateInfo->IsSubsystemProcess)
        {
            KdPrint(("IsSubsystemProcess!!!\n"));
        }
        else
        {
            KdPrint(("Process name = [%S]\n", CreateInfo->ImageFileName->Buffer));
        }

        if (CreateInfo->CommandLine)
        {
            KdPrint(("Command Line = [%S]\n", CreateInfo->CommandLine->Buffer));
        }
    }

    return;
}

NTSTATUS
CallbackRegistryKeys(
    _In_        PVOID CallbackContext,
    _In_opt_    PVOID Argument1,
    _In_opt_    PVOID Argument2
)
{
    UNREFERENCED_PARAMETER(CallbackContext);

    // The callback routine must enclose any access
    // of an output buffer in a try/except block
    NTSTATUS status = STATUS_SUCCESS;
    SIZE_T regNotifyClass = (SIZE_T)Argument1;
    __try
    {
        switch (regNotifyClass)
        {
        case RegNtPreRenameKey:
            status = _HandleRegRenameKey((REG_RENAME_KEY_INFORMATION *)Argument2);
            break;
        default:
            //KdPrint(("Unhandled registry notify class = [%d]\n", regNotifyClass));
            break;
        }
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
        return STATUS_UNSUCCESSFUL;
    }

    return status;
}

VOID
CallbackLoadImage(
    _In_opt_    PUNICODE_STRING FullImageName,
    _In_        HANDLE          ProcessId,  // pid into which image is being mapped
    _In_        PIMAGE_INFO     ImageInfo
)
{
    UNREFERENCED_PARAMETER(ImageInfo);

    // The process ID of the process in which the image has been mapped,
    // but this handle is zero if the newly loaded image is a driver.
    if (ProcessId == 0)
    {
        // The FullImageName parameter can be NULL in cases in which
        // the operating system is unable to obtain the full name
        // of the image at process creation time.
        if (FullImageName)
        {
            KdPrint(("[DRIVER LOADED] Driver name = [%S]\n", FullImageName->Buffer));
        }
    }

    return;
}

/* Static functions */
static
NTSTATUS
_HandleRegRenameKey(
    REG_RENAME_KEY_INFORMATION *RegRenameKeyInfo
)
{
    KdPrint(("\n\n[REGISTRY KEY RENAMED] New name = [%S]\n", RegRenameKeyInfo->NewName->Buffer));

    return STATUS_SUCCESS;
}