#include "dispatch_routines.h"
#include "common.h"
#include "global.h"
#include "commands.h"

/**/
NTSTATUS
IrpCreate(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    UNREFERENCED_PARAMETER(DeviceObject);

    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;

    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
IrpClose(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    UNREFERENCED_PARAMETER(DeviceObject);

    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;

    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
}

/**/
NTSTATUS
IrpDeviceControl(
    _In_ DEVICE_OBJECT  *DeviceObject,
    _In_ IRP            *Irp
)
{
    // TODO: Probabil try except ca avem deaface cu buffer
    // venit de la User Mode...
    UNREFERENCED_PARAMETER(DeviceObject);

    NTSTATUS status = STATUS_SUCCESS;
    IO_STACK_LOCATION *ioStackLocation = IoGetCurrentIrpStackLocation(Irp);

    if (ioStackLocation->Parameters.DeviceIoControl.IoControlCode != MY_FILTER_SEND_COMMAND)
    {
        // Nu stim ce comanda ii asta, nu primim numa MY_FILTER_SEND_COMMAND!!!
        // Termina IRP cu fail si aia e
        status = STATUS_INVALID_DEVICE_REQUEST;
        goto cleanup;
    }

    // Check if the size of the buffer is the expected size
    if (ioStackLocation->Parameters.DeviceIoControl.InputBufferLength != sizeof(DATA_PACKET))
    {
        status = STATUS_BUFFER_TOO_SMALL;
        goto cleanup;
    }

    DATA_PACKET *dataPacket = (DATA_PACKET*)Irp->AssociatedIrp.SystemBuffer;
    status = CommandsDeviceControlIrpHandler(dataPacket);

cleanup:
    {
        Irp->IoStatus.Status = status;
        Irp->IoStatus.Information = 0;

        IoCompleteRequest(Irp, IO_NO_INCREMENT);
    }

    return status;
}