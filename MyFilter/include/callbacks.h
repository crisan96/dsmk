#ifndef _CALLBACKS_H_
#define _CALLBACKS_H_

/*
    Aici se vor declara callback-urile
    ce vor putea fi inregistrate de catre entitatea "hooks"
*/

#include <ntddk.h>
#include <ntdef.h>
#include <wdm.h>

VOID CallbackProcessCreationAndProcessExit(_Inout_ PEPROCESS Process, _In_ HANDLE ProcessId, _Inout_opt_ PS_CREATE_NOTIFY_INFO *CreateInfo);

NTSTATUS CallbackRegistryKeys(_In_ PVOID CallbackContext, _In_opt_ PVOID Argument1, _In_opt_ PVOID Argument2);

VOID CallbackLoadImage(_In_opt_ PUNICODE_STRING FullImageName, _In_ HANDLE ProcessId, _In_ PIMAGE_INFO ImageInfo);

#endif // !_CALLBACKS_H_
