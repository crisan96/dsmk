#ifndef _HOOKS_H_
#define _HOOKS_H_

#include <ntdef.h>
#include <ntddk.h>

/*
    Entitate ce e wrapper peste functiile de kernel ce inregistreaza
    callback-uri pentru a fi chemate in caz ca apar unele evenimente (creare proces, etc.)
    Mi s-o parut mai usor de citit pentru cineva daca ingradesc cu niste wrappere
*/

NTSTATUS HooksInit(VOID);

// De apelat in drver unload pentru a da jos toate callback-urile setate
// From msdn: A driver must remove any callback routines that it registers before it unloads.
NTSTATUS HooksUninit(VOID);

// Inregistreaza un callback ce sa fie chemat la crearea unui proces
// sau la terminarea unui proces
NTSTATUS HooksAddOnProcessCreationAndProcessExit(PCREATE_PROCESS_NOTIFY_ROUTINE_EX Callback);

// Sterge un hook ce a fost inregistrat in prealabil
// folosind functia HooksAddOnProcessCreationAndProcessExit
NTSTATUS HooksRemoveFromProcessCreationAndProcessExit(PCREATE_PROCESS_NOTIFY_ROUTINE_EX Callback);

// Hook pe registry keys. Callback-ul atasat va putea
// monitoriza, bloca sau mofifica operatii facute pe registry keys
NTSTATUS HooksAddOnRegistryKeys(PEX_CALLBACK_FUNCTION Callback);

// Sterge un hook ce a fost inregistrat in prealabil
// folosind functia HooksAddOnRegistryKeys.
NTSTATUS HooksRemoveFromRegistryKeys(PEX_CALLBACK_FUNCTION Callback);

// Adauga un hook pe fiecare incarcare a unui modul in memorie
NTSTATUS HooksAddOnImageLoad(PLOAD_IMAGE_NOTIFY_ROUTINE Callback);

// Sterge un hook ce a fost inregistrat in prealabil
// folosind functia HooksAddOnImageLoad.
NTSTATUS HooksRemoveFromImageLoad(PLOAD_IMAGE_NOTIFY_ROUTINE Callback);

// Hook pe handle operations ale proceselor.
// Hook-ul va apela callback-urile atat inainte de operatia pe handle cat si dupa.
NTSTATUS HooksAddOnProcessHandleOperations(POB_PRE_OPERATION_CALLBACK PreOperationCallback, POB_POST_OPERATION_CALLBACK PostOperationCallback);

#endif // !_HOOKS_H_
