#ifndef _COMMANDS_H_
#define _COMMANDS_H_

#include <ntdef.h>
#include <ntddk.h>
#include "common.h"

//
// Entitate unde se vor expune comenzi ca de
// exemplu protejarea unui process pentru a nu putea
// fi terminat
//

/**/ NTSTATUS CommandsInit(VOID);

/**/ NTSTATUS CommandsUninit(VOID);

/**/ NTSTATUS CommandsProtectProcessFromTermination(UINT64 ProcessID);

// Comunicarea cu aplicatii User-Mode se face prin Device Coltrol.
// Deci, avem nevoie de un handler ce sa se ocupe cu ce trimite utilizatorul.
/**/ NTSTATUS CommandsDeviceControlIrpHandler(DATA_PACKET *DataPacket);

#endif