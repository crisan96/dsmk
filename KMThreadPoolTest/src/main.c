#include <ntddk.h>
#include "simple_km_threadpool.h"

/* Unload routine */
VOID MyUnload(_In_ DRIVER_OBJECT *DriverObject);

typedef struct _MY_DUMMY_STRUCT
{
    UINT32 AiciEste1;
    UINT32 AiciEste7;
}MY_DUMMY_STRUCT;

VOID Job1(VOID *Arg)
{
    UNREFERENCED_PARAMETER(Arg);
    KdPrint(("---JOB1\n"));
    return;
}

VOID Job2(VOID *Arg)
{
    UINT32 *job2Arg = (UINT32 *)Arg;
    KdPrint(("---JOB2 %d\n", *job2Arg));
    return;
}

VOID Job3(VOID *Arg)
{
    MY_DUMMY_STRUCT *dummyStruct = (MY_DUMMY_STRUCT *)Arg;
    KdPrint(("---JOB3 AiciEste1 = %d, AiciEste7 = %d\n", dummyStruct->AiciEste1, dummyStruct->AiciEste7));
    return;
}

NTSTATUS
DriverEntry(
    _In_ DRIVER_OBJECT     *DriverObject,
    _In_ UNICODE_STRING    *RegistryPath
)
{
    UNREFERENCED_PARAMETER(RegistryPath);

    //
    // Set unload routine
    //
    DriverObject->DriverUnload = MyUnload;

    __debugbreak();

    KdPrint(("LOAD!!!!!!!\n"));

    NTSTATUS status = SimpleKmThreadpoolInit(4, 8);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("SimpleKmThreadpoolInit failed (0x%08X)\n", status));
    }

    status = SimpleKmThreadPoolRegisterJob(Job1, NULL, 0);
    if (!NT_SUCCESS(status))
    {
        KdPrint(("SimpleKmThreadPoolRegisterJob failed (0x%08X)\n", status));
    }

    UINT32 aiciEste7 = 7;
    status = SimpleKmThreadPoolRegisterJob(Job2, &aiciEste7, sizeof(aiciEste7));
    if (!NT_SUCCESS(status))
    {
        KdPrint(("SimpleKmThreadPoolRegisterJob failed (0x%08X)\n", status));
    }

    MY_DUMMY_STRUCT myDummyStruct;
    myDummyStruct.AiciEste1 = 1; myDummyStruct.AiciEste7 = 7;
    status = SimpleKmThreadPoolRegisterJob(Job3, &myDummyStruct, sizeof(myDummyStruct));
    if (!NT_SUCCESS(status))
    {
        KdPrint(("SimpleKmThreadPoolRegisterJob failed (0x%08X)\n", status));
    }

    return STATUS_SUCCESS;
}

/* Unload routine */
VOID
MyUnload(
    _In_ DRIVER_OBJECT *DriverObject
)
{
    UNREFERENCED_PARAMETER(DriverObject);

    KdPrint(("UNload!!!!!!!\n"));

    __debugbreak();

    NTSTATUS status = SimpleKmThreadUninit();
    if (!NT_SUCCESS(status))
    {
        KdPrint(("SimpleKmThreadUninit failed (0x%08X)\n", status));
    }

    return;
}
