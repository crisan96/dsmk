#include "simple_km_threadpool.h"
#include <fltKernel.h>

/* Tag-ul folosit pentru alocare */
#define THREAD_POOL_TAG '4BAL'

#define SECONDS_TO_MILLISECONDS(Seconds)    ((Seconds) * 1000)

typedef struct _WORKER
{
#define SIGNAL_CAN_TERMINATE    1
#define SIGNAL_CAN_CONTINUE     0
    __declspec(align(32)) volatile LONG         WorkerShouldTerminate;
    HANDLE                                      WorkerThreadHandle;
    UINT32                                      WorkerId;
}WORKER;

typedef struct _JOB
{
    KMUTEX      Lock;               // Storage for a mutex object must be resident:
                                    // in the device extension of a driver-created device object,
                                    // in the controller extension of a driver-created controller object,
                                    // or in nonpaged pool allocated by the caller.
                                    // => Daca memorie in care va fi incarcat driver-ul nu se scoate in swap
                                    // suntem safe, daca nu... (nu stiu inca...)
    BOOLEAN     IsValid;
    VOID        *Argument;
    PFUNC_Job   FunctionToExecute;
}JOB;

typedef struct _SIMPLE_THREADPOOL_GLOBAL_DATA
{
    struct
    {
        UINT32      MaxJobs;
        KSEMAPHORE  Semaphore;      // Semaforul ne zice cate joburi sunt in "coada".
                                    // Daca semaforul permite 3 thread-uri sa intre in "coada" ar
                                    // insemna ca sunt 3 job-uri de procesat.
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // !!!!!!!!!! aceasi problema ca si la KMUTEX      Lock; de mai sus!!!!!!!!!

        JOB     *ThreadpoolJobs;    // Simularea "cozii" de job-uri. Ar fi de preferat o coada ce sa suporte
                                    // accese concurente/paralele. Pentru simplitate am pus un lacat
                                    // in structura de JOB.
    }Jobs;

    struct
    {
        UINT32   NumberOfWorkerThreads;
        WORKER  *WorkersThreads;
    }Workers;
}SIMPLE_THREADPOOL_GLOBAL_DATA;

/* If this is NULL => component is not inited */
static SIMPLE_THREADPOOL_GLOBAL_DATA *SimpleThreadPoolGlobalData = NULL;

/* IDDLE function. This is where workers threads stay if no JOB to run */
VOID _IddleFunction( VOID* Param );

/* Static functions */
static                  VOID    _CleanupAllTheData(VOID);
static                  JOB*    _FindFirstAvailableJob(VOID);
static __forceinline    BOOLEAN _IsComponentInited(VOID);
static __forceinline    BOOLEAN _CurrentWorkerShouldTerminate(WORKER *WorkerInfo);
static __forceinline    VOID    _SignalWorkerToTerminate(WORKER *WorkerInfo);

/**/
NTSTATUS
SimpleKmThreadpoolInit(
    UINT8   NumberOfWorkerThreads,
    UINT32  MaxNumberOfJobs
)
{
    // Pentru simplitate suportam doar o instanta de threadpool.
    // Pe viitor functia ar putea returna un handle catre o instanta de threadpool.
    if (_IsComponentInited()) { return STATUS_ALREADY_INITIALIZED; }

    NTSTATUS status = STATUS_SUCCESS;

    // Alloc space for the global data structure
    SimpleThreadPoolGlobalData = (SIMPLE_THREADPOOL_GLOBAL_DATA *)ExAllocatePoolWithTag(
        PagedPool,
        sizeof(SimpleThreadPoolGlobalData[0]),
        THREAD_POOL_TAG
    );
    if (!SimpleThreadPoolGlobalData)
    {
        KdPrint(("ExAllocatePoolWithTag failed!\n"));
        status = STATUS_MEMORY_NOT_ALLOCATED;
        goto cleanup;
    }

    // Semaforul pentru joburi. Valoarea semaforului reprezinta
    // numarul de joburi din "coada"
    KeInitializeSemaphore(
        &SimpleThreadPoolGlobalData->Jobs.Semaphore,
        0,                  // currently no job in the "queue"
        MaxNumberOfJobs     // maximum count for semaphore <=> maximum jobs
    );

    // Spatiu pentru "coada" de job-uri.
    SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs = (JOB *)ExAllocatePoolWithTag(
        PagedPool,
        sizeof(SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[0]) * MaxNumberOfJobs,
        THREAD_POOL_TAG
    );
    if (!SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs)
    {
        KdPrint(("ExAllocatePoolWithTag failed!\n"));
        status = STATUS_MEMORY_NOT_ALLOCATED;
        goto cleanup;
    }

    // Iteram "coada" de joburi pentru a semnala ca
    // joburile nu sunt valide si, mai importat, pentru a initializa
    // lacatul job-urilor.
    for (UINT32 i = 0; i < MaxNumberOfJobs; ++i)
    {
        SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[i].IsValid = FALSE;
        KeInitializeMutex(
            &SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[i].Lock,
            0   // reserved, must be set to 0
        );
    }
    SimpleThreadPoolGlobalData->Jobs.MaxJobs = MaxNumberOfJobs;

    // Alocam spatiu unde retinem unele informatii pentru fiecare
    // thread worker in parte
    SimpleThreadPoolGlobalData->Workers.WorkersThreads = (WORKER *)ExAllocatePoolWithTag(
        PagedPool,
        sizeof(SimpleThreadPoolGlobalData->Workers.WorkersThreads[0]) * NumberOfWorkerThreads,
        THREAD_POOL_TAG
    );
    if (!SimpleThreadPoolGlobalData->Workers.WorkersThreads)
    {
        KdPrint(("ExAllocatePoolWithTag failed!\n"));
        status = STATUS_MEMORY_NOT_ALLOCATED;
        goto cleanup;
    }
    for (UINT8 i = 0; i < NumberOfWorkerThreads; ++i)
    {
        SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerShouldTerminate = FALSE;

        status = PsCreateSystemThread(
            &SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerThreadHandle,
            STANDARD_RIGHTS_ALL,                                    // keep it simple..
            NULL,
            NULL,                                                   // we do not create this thread for a UM process..
            NULL,                                                   // this value should be NULL for a driver-created thread.
            _IddleFunction,                                         // where every worker starts its work
            &SimpleThreadPoolGlobalData->Workers.WorkersThreads[i]  // current thread arguments
        );
        if (!NT_SUCCESS(status))
        {
            KdPrint(("PsCreateSystemThread failed with status = 0x%x!\n", status));
            status = STATUS_UNSUCCESSFUL;
            goto cleanup;
        }
    }
    SimpleThreadPoolGlobalData->Workers.NumberOfWorkerThreads = NumberOfWorkerThreads;

cleanup:
    if (!NT_SUCCESS(status))
    {
        /* Cleanup all the data */
        _CleanupAllTheData();
    }

    return status;
}

/**/
NTSTATUS
SimpleKmThreadPoolRegisterJob(
    PFUNC_Job   Job,
    VOID        *Argument,
    UINT32       SizeOfArgument
)
{
    NTSTATUS status = STATUS_SUCCESS;

    if (!_IsComponentInited()) { return STATUS_UNSUCCESSFUL; }

    // Find a free entry in job "queue"
    // Parcurgem doar o data sirul de joburi si
    // APELAM FUNCTIA DE WAIT CU TIMP 0. Deci daca avem
    // ghinionul sa prindem printre primele intrari locuri care atunci se elibereaza,
    // acestea au lacatul luat si trecem peste, => posibil sa raspundem ca nu mai avem loc
    // in "coada" dar defapt sa mai fie... facem asa pt simplitate
    JOB *jobEntry = NULL;
    for (UINT32 entryIndex = 0; entryIndex < SimpleThreadPoolGlobalData->Jobs.MaxJobs; ++entryIndex)
    {
        LARGE_INTEGER timeout = { .QuadPart = 0 };  // Nu asteptam. Daca job-ul e in procesare incercam alta intrare. Asta poate fi problematic.
                                                    // Vezi o explicatie mai sus..
        status = KeWaitForSingleObject(
            &SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].Lock,
            Executive,  // A driver should set this value to Executive...
            KernelMode, // If the given Object is a mutex, the caller must specify KernelMode.
            FALSE,      // Specifies a Boolean value that is TRUE if the wait is alertable and FALSE otherwise.???
            &timeout
        );

        switch (status)
        {
        case STATUS_TIMEOUT:
        {
            continue;
        }
        case STATUS_SUCCESS:
        {
            break;
        }
        default:
        {
            // Ceva eroare...
            KdPrint(("ExAllocatePoolWithTag failed status = 0x%x\n", status));
            status = STATUS_UNSUCCESSFUL;
            goto cleanup;
        }
        }

        // Vezi daca intrarea este folosita sau nu
        if (SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].IsValid)
        {
            KeReleaseMutex(&SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].Lock, FALSE);
            continue;
        }
        else
        {
            // Daca intrarea nu este folosita, am gasit ce cautam: o intrare libera unde
            // putem plasa noul job
            jobEntry = &SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex];
            break;
        }
    }

    if (!jobEntry)
    {
        KdPrint(("!jobEntry...\n"));
        status = STATUS_UNSUCCESSFUL;
        goto cleanup;
    }

    // Register the new job
    if (Argument)
    {
        jobEntry->Argument = ExAllocatePoolWithTag(PagedPool, SizeOfArgument, THREAD_POOL_TAG);
        if (!jobEntry->Argument)
        {
            KdPrint(("ExAllocatePoolWithTag failed status = 0x%x\n", status));
            KeReleaseMutex(&jobEntry->Lock, FALSE);
            status = STATUS_UNSUCCESSFUL;
            goto cleanup;
        }
        memcpy(jobEntry->Argument, Argument, SizeOfArgument);
    }
    else
    {
        jobEntry->Argument = NULL;
    }

    jobEntry->FunctionToExecute = Job;
    jobEntry->IsValid = TRUE;
    KeReleaseMutex(&jobEntry->Lock, FALSE);

    // Signal the semaphore that we have one new job
    KeReleaseSemaphore(&SimpleThreadPoolGlobalData->Jobs.Semaphore, 0, 1, FALSE);

cleanup:
    return status;
}

/**/
NTSTATUS
SimpleKmThreadUninit(
    VOID
)
{
    if (!_IsComponentInited()) { return STATUS_UNSUCCESSFUL; }

    for (UINT8 i = 0; i < SimpleThreadPoolGlobalData->Workers.NumberOfWorkerThreads; ++i)
    {
        _SignalWorkerToTerminate(&SimpleThreadPoolGlobalData->Workers.WorkersThreads[i]);
        ZwWaitForSingleObject(SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerThreadHandle, FALSE, NULL);
    }

    /* Cleanup all the data */
    _CleanupAllTheData();

    return STATUS_SUCCESS;
}

/* IDDLE function. This is where workers threads stay if no JOB to run */
VOID
_IddleFunction(
    VOID *Param
)
{
    WORKER *currentWorkerInfo = (WORKER *)Param;

    while (TRUE)
    {
        while (TRUE)    // In loc de while asta era mai bine sa astepte aici dupa
                        // doua evenimente iar unul dintre ele sa fie un eveniment global
                        // de terminare a ThreadPool-ului...
        {
            LARGE_INTEGER timeout = { .QuadPart = 10000000 };   // iesim la fiecare secunda sa verificam daca
                                                                // nu cumva trebuie sa terminam threadul...
                                                                // ar fi mai bine cum am scris mai sus..
            NTSTATUS status = KeWaitForSingleObject(
                &SimpleThreadPoolGlobalData->Jobs.Semaphore,
                Executive,
                KernelMode,
                FALSE,
                &timeout
            );
            if (_CurrentWorkerShouldTerminate(currentWorkerInfo)) { goto _end_this_thread; }

            if (status == STATUS_SUCCESS)
            {
                break;
            }
            else if (status == STATUS_TIMEOUT)
            {
                continue;
            }
            else
            {
                KdPrint(("Nasol....."));
                goto _end_this_thread;
            }
        }

        //
        // We have work to do
        //

        // Gaseste primul job valid care trebuie executat
        // Primim inapoi pointer la job si suntem ASIGURATI CA LACATUL JOB-ULUI E DEJA LUAT
        JOB *jobToExecute = _FindFirstAvailableJob();
        if (!jobToExecute)
        {
            // Nu prea ar trebui sa ajungem aici...
            continue;
        }

        // Executa job-ul cu argumentele date..
        jobToExecute->FunctionToExecute(jobToExecute->Argument);

        // Nu uita sa faci release la lacatul job-ului
        // + sa faci release la memoria argumentului
        // + sa semnalezi ca job-ul e gata => nu mai e valid
        if (jobToExecute->Argument) { ExFreePoolWithTag(jobToExecute->Argument, THREAD_POOL_TAG); }
        jobToExecute->IsValid = FALSE;
        KeReleaseMutex(&jobToExecute->Lock, FALSE);
    }

_end_this_thread:
    KdPrint(("This thread terminate!!!\n"));
    PsTerminateSystemThread(STATUS_SUCCESS);
}

/* Static functions */
static
__forceinline
BOOLEAN
_IsComponentInited(
    VOID
)
{
    return SimpleThreadPoolGlobalData != NULL;
}

static
__forceinline
BOOLEAN
_CurrentWorkerShouldTerminate(
    WORKER *WorkerInfo
)
{
    return
        InterlockedCompareExchange(&WorkerInfo->WorkerShouldTerminate, SIGNAL_CAN_CONTINUE, SIGNAL_CAN_TERMINATE)
        == SIGNAL_CAN_TERMINATE ?
        TRUE : FALSE;
}

static
JOB*
_FindFirstAvailableJob(
    VOID
)
{
    for (UINT32 entryIndex = 0; entryIndex < SimpleThreadPoolGlobalData->Jobs.MaxJobs; ++entryIndex)
    {
        LARGE_INTEGER timeout = { .QuadPart = 0 };  // Nu asteptam. Daca job-ul e in procesare incercam alta intrare. Asta poate fi problematic.
                                                    // Vezi o explicatie mai sus..
        NTSTATUS waitStatus = KeWaitForSingleObject(
            &SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].Lock,
            Executive,
            KernelMode,
            FALSE,
            &timeout
        );
        switch (waitStatus)
        {
        case STATUS_TIMEOUT:
        {
            continue;
        }
        case STATUS_SUCCESS:
        {
            break;
        }
        default:
        {
            // Ceva eroare...
            return NULL;
        }
        }

        if (SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex].IsValid)
        {
            return &SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[entryIndex];
        }
    }

    return NULL;
}

static
__forceinline
VOID
_SignalWorkerToTerminate(
    WORKER *WorkerInfo
)
{
    InterlockedExchange(&WorkerInfo->WorkerShouldTerminate, SIGNAL_CAN_TERMINATE);
}

static
VOID
_CleanupAllTheData(
    VOID
)
{
    if (SimpleThreadPoolGlobalData)
    {
        // Workers
        if (SimpleThreadPoolGlobalData->Workers.WorkersThreads)
        {
            for(UINT8 i = 0; i < SimpleThreadPoolGlobalData->Workers.NumberOfWorkerThreads; ++i)
            {
                //CloseHandle(SimpleThreadPoolGlobalData->Workers.WorkersThreads[i].WorkerThreadHandle);
            }
            ExFreePoolWithTag(SimpleThreadPoolGlobalData->Workers.WorkersThreads, THREAD_POOL_TAG);
        }

        // Jobs
        if (SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs)
        {
            for (UINT32 i = 0; i < SimpleThreadPoolGlobalData->Jobs.MaxJobs; ++i)
            {
                //CloseHandle(SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs[i].Lock);
            }
            //CloseHandle(SimpleThreadPoolGlobalData->Jobs.Semaphore);
            ExFreePoolWithTag(SimpleThreadPoolGlobalData->Jobs.ThreadpoolJobs, THREAD_POOL_TAG);
        }

        ExFreePoolWithTag(SimpleThreadPoolGlobalData, THREAD_POOL_TAG);
    }

    // This will tell to the users that the component is now uninited
    SimpleThreadPoolGlobalData = NULL;
}